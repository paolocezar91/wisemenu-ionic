import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthProvider } from './auth';
import 'rxjs/add/operator/map';
import { ENV } from '../env/environment-variables.token';

/*
  Generated class for the Favoritos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FavoritosProvider {

  favorito: any
  favoritos: any

  constructor(
    public http: Http,
    private authProvider: AuthProvider,
    @Inject(ENV) public ENV
  ) {
    console.log('Hello Filtros Provider');
  }

  newFavorito(receita_id){
    return {
      "user_id": this.authProvider.getCurrentUser()._id,
      "receita_id": receita_id
    };
  }

  getOne(data, options){ 

    return new Promise(resolve => {
 
      let headers = new Headers();
      headers.append('Authorization', this.authProvider.getToken());

      this.http.post(this.ENV.API_URL + 'favoritos/one', {data: data, options: options}, {headers: headers})
        .map(res => res.json())
        .subscribe(favorito => {
          this.favorito = favorito;
          resolve(favorito);          
        });
    });
 
  }

  getAll(data, options){ 
    return new Promise(resolve => {
 
      let headers = new Headers();
      headers.append('Authorization', this.authProvider.getToken());

      this.http.post(this.ENV.API_URL + 'favoritos', {data: data, options: options}, {headers: headers})
        .map(res => res.json())
        .subscribe(favoritos => {
          this.favoritos = favoritos;
          resolve(favoritos);
        });
    });
 
  }

  update(data, options){
    return new Promise(resolve => {
 
      let headers = new Headers();
      headers.append('Authorization', this.authProvider.getToken());

      this.http.post(this.ENV.API_URL + 'favoritos/update', {data: data, options: options}, {headers: headers})
        .map(res => res.json())
        .subscribe(favorito => {
          resolve(favorito);
        });
    });

  }


}
