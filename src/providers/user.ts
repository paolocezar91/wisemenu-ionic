import { Injectable } from "@angular/core"
import { Http, Headers } from "@angular/http"
import { AuthProvider } from "./auth"
import "rxjs/add/operator/map"

@Injectable()
export class PerfilProvider {

	constructor(
		public http: Http,
		public authProvider: AuthProvider
	) {}

	// user um objeto para busca no mongodb
	getUser(user) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.authProvider.token)

			this.http.post("http://localhost:8080/api/user", JSON.stringify(user), {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	getPerfil(perfil) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.authProvider.token)

			this.http.post("http://localhost:8080/api/user/perfil", JSON.stringify(perfil), {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}
}
