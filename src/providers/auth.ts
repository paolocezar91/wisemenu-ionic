import { Injectable, Inject } from "@angular/core"
import { Http, Headers } from "@angular/http"
import { Storage } from "@ionic/storage"
import "rxjs/add/operator/map"
import { ENV } from "../env/environment-variables.token"

@Injectable()
export class AuthProvider {
	token: any
	private currentUser: any

	constructor(
		public http: Http,
		public storage: Storage,
		@Inject(ENV) public ENV
		) {
		console.log("Hello Auth Provider")
	}

	checkAuthentication() {
		return new Promise((resolve, reject) => { // Carrega o token
			this.storage.get("token").then((value) => {
				this.token = value

				// Cria uma cabeçalho de autorização para checar no servidor
				const headers = new Headers()
				headers.append("Authorization", this.token)

				this.http.get(this.ENV.API_URL + "auth/protected", {headers})
					.map(res => res.json())
					.subscribe(res => {
						// Pega os dados locais do usuário
						this.storage.set("currentUser", res.user)
							.then(user => {
								this.currentUser = user
								resolve(res.user)
							})
					}, (err) => {
						reject(err)
					})
			})
		})
	}

	createAccount(details) {
		return new Promise((resolve, reject) => {
			const headers = new Headers()
			headers.append("Content-Type", "application/json")

			this.http.post(this.ENV.API_URL + "auth/register", JSON.stringify(details), {headers})
			.map(res => res.json())
			.subscribe(res => {
				this.storage.set("token", res.token)
					.then(token => {
						this.token = token
						this.storage.set("currentUser", res.user)
							.then(user => {
								this.currentUser = user
								resolve(res)
							})
					})
				resolve(res)
			}, (err) => {
				reject(err)
			})
		})
	}

	createGuestAccount(details) {
		return new Promise((resolve, reject) => {
			const headers = new Headers()
			headers.append("Content-Type", "application/json")

			this.http.post(this.ENV.API_URL + "auth/guest", JSON.stringify(details), {headers})
			.map(res => res.json())
			.subscribe(res => {
				this.storage.set("token", res.token)
					.then(token => {
						this.token = token
						this.storage.set("currentUser", res.user)
							.then(user => {
								this.currentUser = user
								resolve(res)
							})
					})
			}, (err) => {
				reject(err)
			})

		})

	}

	createFBAccount(details) {
		return new Promise((resolve, reject) => {

			const headers = new Headers()
			headers.append("Content-Type", "application/json")

			this.http.post(this.ENV.API_URL + "auth/facebook", JSON.stringify(details), {headers})
			.map(res => res.json())
			.subscribe(res => {
				this.storage.set("token", res.token)
					.then(token => {
						this.token = token
						this.storage.set("currentUser", res.user)
							.then(user => {
								this.currentUser = user
								resolve(res)
							})
					})
			}, (err) => {
				reject(err)
			})

		})
	}

	login(credentials) {
		return new Promise((resolve, reject) => {
			const headers = new Headers()
			headers.append("Content-Type", "application/json")

			this.http.post(this.ENV.API_URL + "auth/login", JSON.stringify(credentials), {headers})
			.map(res => res.json())
			.subscribe(res => {
				this.storage.set("token", res.token)
					.then(token => {
						this.token = token
						this.storage.set("currentUser", res.user)
							.then(user => {
								this.currentUser = user
								resolve(res)
							})
					})
			}, (err) => {
				console.log(err)
				reject(err)
			})

		})

	}

	resetPassword(data) {
		return new Promise((resolve, reject) => {
			const headers = new Headers()
			headers.append("Authorization", this.token)

			data.email = this.getCurrentUser().email

			this.http.post(this.ENV.API_URL + "auth/resetPassword", data, {headers})
			.map(res => res.json())
			.subscribe(res => {
				resolve(res)
			}, (err) => {
				reject(err)
			})
		})
	}

	logout() {
		return new Promise((resolve, reject) => {
			this.storage.set("token", "")
					.then(token => {
						this.token = token
						this.storage.set("currentUser", "")
							.then(user => {
								this.currentUser = user
								resolve(true)
							})
					})
		});
	}

	currentUserRoleCheck(check) {
		// verifica se o usuário já foi salvo no storage e testa a role
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				if (this.currentUser) {
					if (this.currentUser.role === check) {
						resolve(true)
					} else {
						resolve(false)
					}
				}
			}, 500)
		});
	}

	getCurrentUser() {
		return this.currentUser
	}

	getToken() {
		return this.token
	}
}