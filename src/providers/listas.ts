import { Injectable, Inject } from "@angular/core"
import { Http, Headers } from "@angular/http"
import { AuthProvider } from "./auth"
import "rxjs/add/operator/map"
import { ENV } from "../env/environment-variables.token"
/*
	Generated class for the Listas provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ListasProvider {

	listas: any
	lista: any

	constructor(
		public http: Http,
		private ap: AuthProvider,
		@Inject(ENV) public ENV
	) {
		console.log("Hello Listas Provider")
	}



	getAll(data, options?) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.ap.getToken())

			this.http.post(this.ENV.API_URL + "listas", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(listas => {
					this.listas = listas
					resolve(this.listas)
				})
		})
	}


	getOne(data, options?) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.ap.getToken())

			this.http.post(this.ENV.API_URL + "listas/one", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(lista => {
					this.lista = lista
					resolve(this.lista)
				})
		})
	}

	create(data, options?) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.ap.getToken())

			this.http.post(this.ENV.API_URL + "listas/create", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(lista => {
					this.lista = lista
					resolve(this.lista)
				})
		})

	}

	update(data, options?) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.ap.getToken())

			this.http.post(this.ENV.API_URL + "listas/update", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	delete(data, options?) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.ap.getToken())

			this.http.post(this.ENV.API_URL + "listas/delete", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	newLista(c) {
		return new Promise(resolve => {
			let titulo = c.titulo

			console.log("c.gerado", c.gerado)
			console.log( (c.gerado - 1)  > 0 )
			if (c.gerado - 1 > 0) {
				titulo = titulo + " (" + (c.gerado - 1) + ")"
			}

			const data = {
				titulo,
				receitas: this.extractReceitas(c),
				user_id: this.ap.getCurrentUser()._id,
				cardapio_id: c._id
			}
			resolve(data)
		})
	}

	extractReceitas(c) {
		const allReceitas = []
		for (const d of c.dias) {
			for (const r of d.refeicoes) {
				for (const re of r.receitas) {
					allReceitas.push({_id: re._id, porcoes: re.porcoes !== undefined ? re.porcoes : c.porcoes})
				}
			}
		}
		return allReceitas
	}

}
