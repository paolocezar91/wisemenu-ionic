import { Injectable } from "@angular/core";
import { AuthProvider } from "./auth";
import Moment from "moment";

@Injectable()
export class CurrentCardapiosProvider {

	private cardapio: any;
	private cardapioIndex: any;
	private refeicaoIndex: any;
	private modelCardapioDia: any;

	constructor(
		private authProvider: AuthProvider
	) {}

	newCardapio(data) {
		this.resetProvider();
		this.cardapio = {
			titulo: data.titulo,
			de: data.de,
			ate: data.ate,
			porcoes: data.porcoes,
			diasCounter: data.diasCounter,
			dias: data.dias,
			user_id: this.authProvider.getCurrentUser()._id,
			gerado: 0
		};

		if (this.cardapio.dias.length < data.diasCounter) {

			let de: any;
			for (let i = this.cardapio.dias.length; i < data.diasCounter; i++) {
				de = Moment(data.de);
				this.cardapio.dias[i] = {
					ordem: i,
					data: de.add(i, "days").toJSON(),
					refeicoes: [
						{
							tag: "cafe",
							receitas: [],
							touched: false
						}, {
							tag: "lanche-manha",
							receitas: [],
							touched: false
						}, {
							tag: "almoco",
							receitas: [],
							touched: false
						}, {
							tag: "lanche-tarde",
							receitas: [],
							touched: false
						}, {
							tag: "jantar",
							receitas: []
						}, {
							tag: "ceia",
							receitas: [],
							touched: false
						}
					]
				};
			}
		} else if (this.cardapio.dias.length > data.diasCounter) {
			// i = 5; i >= 3; --i
			for (let i = (- 1 + this.cardapio.dias.length); i >= data.diasCounter; --i) {
				this.cardapio.dias.splice(i, 1);
			}
		}

		return this.cardapio;
	}

	resetRefeicao(dia) {
		this.cardapio.dias[dia].refeicoes = [
			{
				tag: "cafe",
				receitas: [],
				touched: false
			}, {
				tag: "lanche-manha",
				receitas: [],
				touched: false
			}, {
				tag: "almoco",
				receitas: [],
				touched: false
			}, {
				tag: "lanche-tarde",
				receitas: [],
				touched: false
			}, {
				tag: "jantar",
				receitas: []
			}, {
				tag: "ceia",
				receitas: [],
				touched: false
			}
		]
		return this.cardapio.dias[dia].refeicoes
	}

	resetProvider() {
		this.cardapio = false;
		this.cardapioIndex = false;
		this.refeicaoIndex = false;
		this.modelCardapioDia = false;
	}

	setCardapio(c) { return this.cardapio = c; }
	getCardapio() { return this.cardapio; }
	setModelCardapioDia(c) { return this.modelCardapioDia = c; }
	getModelCardapioDia() { return this.modelCardapioDia; }
	setCardapioIndex(i) { return this.cardapioIndex = i; }
	getCardapioIndex() { return this.cardapioIndex; }
	setRefeicaoIndex(i) { return this.refeicaoIndex = i; }
	getRefeicaoIndex() { return this.refeicaoIndex; }
}
