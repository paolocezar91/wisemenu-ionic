import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { AlertController, ToastController, ActionSheetController } from "ionic-angular"
import { isEqual } from "lodash";

/*
	Generated class for the UtilsProvider provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular DI.
*/
@Injectable()
export class UtilsProvider {

	constructor(
		private alertCtrl: AlertController,
		private toastCtrl: ToastController,
		private actionSheetCtrl: ActionSheetController
	) {
		console.log("Hello UtilsProvider Provider");
	}

	isDifferent(original, clone) {
		if (clone === undefined) {return false}
		return !isEqual(this.clone(original), clone)
	}

	clone(original) {
		return JSON.parse(JSON.stringify(original))
	}

	alert(options) {
		const alert = this.alertCtrl.create({
			title: options.title,
			message: options.message,
			buttons: options.buttons,
			inputs: options.inputs
		})
		alert.present()
	}

	toast(options) {
		const toast = this.toastCtrl.create({
			message: options.message,
			duration: options.duration || 2000
		})
		toast.present()
	}

	actionSheet(options) {
		const actionSheet = this.actionSheetCtrl.create({
			title: options.title,
			buttons: options.buttons
		})
		actionSheet.present()
	}

}
