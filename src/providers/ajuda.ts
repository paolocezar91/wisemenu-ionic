import { Injectable, Inject } from "@angular/core";
import { Http, Headers } from "@angular/http";
import "rxjs/add/operator/map";
import { ENV } from "../env/environment-variables.token";

/*
	Generated class for the Ajuda provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AjudaProvider {

	constructor(
		public http: Http,
		@Inject(ENV) public ENV
	) {
		console.log("Hello Ajuda Provider");
	}

	getAll(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			// headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "ajudas", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});
	}

}
