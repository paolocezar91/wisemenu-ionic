import { Injectable, Inject } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { AuthProvider } from "./auth";
import "rxjs/add/operator/map";
import { ENV } from "../env/environment-variables.token";

/*
	Generated class for the Filtros provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FiltrosProvider {

	filtro: any
	filtros: any

	constructor(
		public http: Http,
		private authProvider: AuthProvider,
		@Inject(ENV) public ENV
	) {
		console.log("Hello Filtros Provider");
	}

	newFilter() {
		return {
			"user_id": this.authProvider.getCurrentUser()._id,
			"restricoes" : {
				"tempo_de_preparo" : 120,
				"dificuldade" : 5,
				"populares" : false,
				"avaliados" : false,
				"tags" : []
			},
			"preferencias" : {
					"avaliados": true,
					"favoritos": true,
					"populares": false,
					"refeicao": false
			},
		};
	}

	getOne(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.getToken());

			this.http.post(this.ENV.API_URL + "filtros/one", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(filtro => {
					this.filtro = filtro;
					if (filtro) {
						resolve(filtro);
					} else {
						resolve(this.newFilter());
					}
				});
		});
	}

	getAll(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.getToken());

			this.http.post(this.ENV.API_URL + "filtros", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(filtros => {
					this.filtros = filtros;
					resolve(filtros);
				});
		});
	}

	update(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.getToken());

			this.http.post(this.ENV.API_URL + "filtros/update", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(filtro => {
					resolve(filtro);
				});
		});

	}


}
