import { Http, Headers } from "@angular/http";
import { Injectable, Inject } from "@angular/core";
import { AuthProvider } from "./auth";
import "rxjs/add/operator/map"
import { ENV } from "../env/environment-variables.token";

@Injectable()
export class ProdutosProvider {

	produto: any

	constructor(
		public http: Http,
		private ap: AuthProvider,
		@Inject(ENV) public ENV
	) {

	}

	getResults(search) {
		const data = {}
		const options = {select: "titulo"}

		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "produtos", {data, options}, {headers})
				.map(res => {
					return res.json().filter(item => {
						if (item) {
							return item.titulo.toLowerCase().startsWith(search.toLowerCase())
						}
					});
				})
				.subscribe(data => {
					const filter = data.filter(item => {
						if (item) {
							return item.titulo.toLowerCase().startsWith(search.toLowerCase())
						}
					});

					resolve(filter);
				});
			});
	}

	create(data, options?) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "produtos/create", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(produto => {
					this.produto = produto;
					resolve(this.produto);
				});
		});

	}
}