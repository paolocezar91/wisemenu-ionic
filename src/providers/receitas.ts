import { Injectable, Inject } from "@angular/core"
import { Http, Headers } from "@angular/http"
import { AuthProvider } from "./auth"
import "rxjs/add/operator/map"
import { ENV } from "../env/environment-variables.token"

@Injectable()
export class ReceitasProvider {

	constructor(
		public http: Http,
		public authProvider: AuthProvider,
		@Inject(ENV) public ENV
	) {
	}

	getReceitas(data, options) {
		return new Promise<any>(resolve => {

			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "receitas", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	searchReceitas(data){
		return new Promise<any>(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "receitas/search", {data}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		});

	}

	getReceitasFiltered(data, options) {
		return new Promise(resolve => {

			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "receitas/filtered", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	// Receita única
	getReceita(data, options) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "receitas/one", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	avaliarReceita(data, options) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "receitas/avaliar", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	createOrUpdateReceita(data, options) {
		return new Promise(resolve => {

			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "receitas/createOrUpdate", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	removeReceita(data, options = {}) {
		return new Promise(resolve => {
			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "receitas/remove", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}

	upload(params: any, files: Array<File>, path) {
		return new Promise((resolve, reject) => {
			const formData: any = new FormData()
			const xhr = new XMLHttpRequest()

			formData.append("attr", params.attr)

			for (let i = 0; i < files.length; i++) {
				formData.append("upload", files[i], files[i].name)
			}
			xhr.onreadystatechange = function () {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						resolve(xhr.response)
					} else {
						reject(xhr.response)
					}
				}
			}
			xhr.open("POST", this.ENV.API_URL + path, true)
			xhr.setRequestHeader("Authorization", this.authProvider.getToken())

			xhr.send(formData)
		})
	}

	uploadXls(params: any, files: Array<File>) {
		return new Promise((resolve, reject) => {
			return this.upload(params, files, "receitas/uploadXls")
				.then((result) => {
					resolve(result)
				})
				.catch((err) => {
					reject(err)
				})
		})
	}

	uploadImg(params: any, files: Array<File>) {
		return new Promise((resolve, reject) => {
			return this.upload(params, files, "receitas/uploadImage")
		})
	}
}