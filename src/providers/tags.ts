import { Injectable, Inject } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { AuthProvider } from "./auth";
import "rxjs/add/operator/map";
import { ENV } from "../env/environment-variables.token";

/*
	Generated class for the Tags provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TagsProvider {

	constructor(
		public http: Http,
		private authProvider: AuthProvider,
		@Inject(ENV) public ENV
	) {
		console.log("Hello Tags Provider");
	}

	getAll(data, options) {
		return new Promise<any>(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.token);

			this.http.post(this.ENV.API_URL + "tags", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});
	}

	getOne(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.token);

			this.http.post(this.ENV.API_URL + "tags/one", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		})
	}

	createOrUpdate(data, options) {
		return new Promise(resolve => {

			const headers = new Headers()
			headers.append("Authorization", this.authProvider.getToken())

			this.http.post(this.ENV.API_URL + "tags/createOrUpdate", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data)
				})
		})
	}
}
