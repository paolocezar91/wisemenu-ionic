import { Injectable, Inject } from "@angular/core";
import { Http, Headers } from "@angular/http";
import "rxjs/add/operator/map";
import { AuthProvider } from "./auth";
import { ENV } from "../env/environment-variables.token";

/*
	Generated class for the Refeicoes provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RefeicoesProvider {

	constructor(
		public http: Http,
		public authProvider: AuthProvider,
		@Inject(ENV) public ENV
	) {
		console.log("Hello Refeicoes Provider");
	}

	// options serão queries para o banco de dados
	getRefeicoes(options) {
		return [
			{
				ordem: 0,
				icone: "cafe.png",
				titulo: "Café-da-manhã",
				tag: "58f9076457a4bd2cfca9e85e"
			},
			{
				ordem: 1,
				icone: "lanchemanha.png",
				titulo: "Lanche da manhã",
				tag: "58f9076457a4bd2cfca9e862"
			},
			{
				ordem: 2,
				icone: "almoco.png",
				titulo: "Almoço",
				tag: "58f9076457a4bd2cfca9e85f"
			},
			{
				ordem: 3,
				icone: "lanchetarde.png",
				titulo: "Lanche da tarde",
				tag: "58f9076457a4bd2cfca9e862"
			},
			{
				ordem: 4,
				icone: "jantar.png",
				titulo: "Jantar",
				tag: "58f9076457a4bd2cfca9e860"
			},
			{
				ordem: 5,
				icone: "ceia.png",
				titulo: "Ceia",
				tag: "58f9076457a4bd2cfca9e861"
			},
		];
	}

	// Refeicao única
	getRefeicao(receita_id) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.token);

			this.http.get(this.ENV.API_URL + "refeicoes/" + receita_id, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});
	}

}
