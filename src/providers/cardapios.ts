import { Injectable, Inject } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { AuthProvider } from "./auth";
import "rxjs/add/operator/map";
import { ENV } from "../env/environment-variables.token";
/*
	Generated class for the Cardapios provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CardapiosProvider {

	constructor(
		public http: Http,
		private ap: AuthProvider,
		@Inject(ENV) public ENV
	) {
		console.log("Hello Cardapios Provider");
	}

	getAll(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "cardapios", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});
	}


	getOne(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "cardapios/one", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});
	}

	create(data, options) {

		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "cardapios/create", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});

	}

	update(data, options) {

		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "cardapios/update", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});

	}

	delete(data, options?) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.ap.getToken());

			this.http.post(this.ENV.API_URL + "cardapios/delete", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(data => {
					resolve(data);
				});
		});
	}

}
