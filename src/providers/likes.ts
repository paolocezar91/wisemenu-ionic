import { Injectable, Inject } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { AuthProvider } from "./auth";
import "rxjs/add/operator/map";
import { ENV } from "../env/environment-variables.token";

/*
	Generated class for the Likes provider.

	See https://angular.io/docs/ts/latest/guide/dependency-injection.html
	for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LikesProvider {

	constructor(
		public http: Http,
		private authProvider: AuthProvider,
		@Inject(ENV) public ENV
	) {
		console.log("Hello Filtros Provider");
	}

	newLike(receita_id) {
		return {
			"user_id": this.authProvider.getCurrentUser()._id,
			"receita_id": receita_id
		};
	}

	getOne(data, options) {

		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.getToken());

			this.http.post(this.ENV.API_URL + "likes/one", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(like => {
					resolve(like);
				});
		});
	}

	getAll(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.getToken());

			this.http.post(this.ENV.API_URL + "likes", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(likes => {
					resolve(likes);
				});
		});
	}

	update(data, options) {
		return new Promise(resolve => {
			const headers = new Headers();
			headers.append("Authorization", this.authProvider.getToken());

			this.http.post(this.ENV.API_URL + "likes/update", {data, options}, {headers})
				.map(res => res.json())
				.subscribe(like => {
					resolve(like);
				});
		});
	}


}
