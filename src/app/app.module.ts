import { NgModule, ErrorHandler, LOCALE_ID } from "@angular/core";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { BrowserModule } from "@angular/platform-browser";
import { IonicStorageModule } from "@ionic/storage";
import { HttpModule } from "@angular/http";
import { MyApp } from "./app.component";
import { AuthProvider } from "../providers/auth";
import { CurrentCardapiosProvider } from "../providers/current-cardapios";
import { EnvironmentsModule } from "../env/environment-variables.module";
import { ComponentsModule } from "../components/components.module";
import { UtilsProvider } from "../providers/utils";
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask/src/currency-mask.config";

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
	align: "right",
	allowNegative: false,
	allowZero: true,
	decimal: ",",
	precision: 2,
	prefix: "R$ ",
	suffix: "",
	thousands: "."
};

@NgModule({
	declarations: [
		MyApp,
	],
	imports: [
		BrowserModule,
		HttpModule,
		IonicModule.forRoot(MyApp, {
			scrollPadding: false,
			scrollAssist: true,
			autoFocusAssist: false,
			backButtonText: "",
			backButtonIcon: "return-left",
			modalEnter: "modal-slide-in",
			modalLeave: "modal-slide-out",
			pageTransition: "md",
			iconMode: "md",
			mode: "md"
		}),
		IonicStorageModule.forRoot(),
		EnvironmentsModule,
		ComponentsModule,
		CurrencyMaskModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp
	],
	providers: [
		{ provide: LOCALE_ID, useValue: "pt-BR" },
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		AuthProvider,
		CurrentCardapiosProvider,
		UtilsProvider,
		{ provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
	]
})
export class AppModule {}
