import { Component, ViewChild, Inject } from "@angular/core"
import { Platform, MenuController, Nav, LoadingController } from "ionic-angular"
import { StatusBar } from "@ionic-native/status-bar"
import { AuthProvider } from "../providers/auth"
import { ENV } from "../env/environment-variables.token"

@Component({
	templateUrl: "app.html",
	providers: [StatusBar]
})
export class MyApp {
	@ViewChild(Nav) nav: Nav

	rootPage: any = "SplashscreenPage"
	loading: any
	menu: any

	constructor(
		public platform: Platform,
		public ap: AuthProvider,
		private loadingCtrl: LoadingController,
		private menuCtrl: MenuController,
		private statusBar: StatusBar,
		@Inject(ENV) private ENV
	) {
		try {
			this.showLoader()
			this.ap.checkAuthentication().then((result) => {
				this.rootPage = "HomePage"
				this.loading.dismiss()
			}).catch((err) => {
				console.log("Não está autorizado")
				this.rootPage = "SplashscreenPage"
				this.loading.dismiss()
			});

			this.platform.ready().then(() => {
				// Okay, so the platform is ready and our plugins are available.
				// Here you can do any higher level native things you might need.
				this.statusBar.styleDefault()
				this.statusBar.show()


				// Check if already authenticated
			})
		} catch (err) {
			console.log(err)
			this.loading.dismiss()
		}
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando...",
		})
		this.loading.present()
	}

	returnMenu() {
		const menu = [
			{
				id: "home",
				icon: "fa-home",
				title: "Início",
				callback: () => this.pushPage("HomePage")
			},
			{
				id: "montar-cardapio",
				icon: "fa-file-o",
				title: "Montar cardápio",
				callback: () => this.pushPage("CardapioPage")
			}, {
				id: "meu-perfil",
				icon: "fa-user",
				title: "Meu perfil",
				callback: () => this.pushPage("PerfilPage")
			}, {
				id: "meus-cardapios",
				icon: "fa-files-o fa-flip-horizontal",
				title: "Meus cardápios",
				callback: () => this.pushPage("CardapiosPage")
			}, {
				id: "lista-de-compras",
				icon: "fa-align-left",
				title: "Lista de Compras",
				callback: () => this.pushPage("ListasPage")
			}, {
				id: "minhas-avaliacoes",
				icon: "fa-star",
				title: "Minhas avaliações",
				callback: () => this.pushPage("AvaliacoesPage")
			}, {
				id: "preferencias-restricoes",
				icon: "fa-filter fa-flip-horizontal",
				title: "Filtros",
				callback: () => this.pushPage("FiltrosPage")
			}, {
				id: "dicas",
				icon: "fa-lightbulb-o",
				title: "Dicas de preparo",
				callback: () => {this.pushPage("DicasPage")}
			}, {
				id: "ajuda",
				icon: "fa-question-circle",
				title: "Ajuda",
				callback: () => this.pushPage("AjudaPage")
			}, {
				id: "sair",
				icon: "fa-sign-out",
				title: "Sair",
				callback: () => {
					this.logout()
					this.menuCtrl.close()
				}
			}
		]
		if (this.ENV.ADMIN) {
			menu.push({
				id: "admin",
				icon: "fa-user-secret",
				title: "Admin",
				callback: () => this.pushPage("AdminHomePage")
			})
		}
		return menu
	}

	closeMenu() {
		this.menuCtrl.close()
	}

	pushPage(p) {
		this.nav.setRoot(p)
		this.closeMenu()
	}

	logout() {
		this.ap.logout().then((result) => {
			if (result) {
				this.nav.setRoot("SplashscreenPage")
			}
		})
	}

}


