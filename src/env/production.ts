export const prodVariables = {
	PRODUCTION: true,
	API_URL: "https://www.tech4archi.com/api/",
	RESOURCES_PATH: "https://www.tech4archi.com/resources/",
	DOWNLOAD_PATH: "https://www.tech4archi.com/downloads/",
	ADMIN: false
}