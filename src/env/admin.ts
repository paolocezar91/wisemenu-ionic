export const adminVariables = {
	PRODUCTION : false,
	API_URL    : "http://54.149.174.96:8080/api/",
	RESOURCES_PATH : "http://54.149.174.96:8080/resources/",
	DOWNLOAD_PATH: "http://54.149.174.96:8080/downloads/",
	ADMIN: true
};