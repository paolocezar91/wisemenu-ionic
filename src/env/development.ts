export const devVariables = {
	PRODUCTION : false,
	API_URL    : "http://localhost:8080/api/",
	RESOURCES_PATH : "http://localhost:8080/resources/",
	DOWNLOAD_PATH: "http://localhost:8080/uploads/",
	ADMIN: true
}