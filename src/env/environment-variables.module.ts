import { NgModule } from "@angular/core"
import { ENV } from "./environment-variables.token"
import { devVariables } from "./development"
import { adminVariables } from "./admin"
import { prodVariables } from "./production"
import { testVariables } from "./testing"

declare const process: any // Typescript compiler will complain without this

export function environmentFactory() {
	const NODE_ENV = !(typeof process.env.NODE_ENV === "undefined") ? process.env.NODE_ENV : "dev"
	console.log("Using " + NODE_ENV + " variables.")

		return NODE_ENV === "admin" ?
				adminVariables :
			NODE_ENV === "prod" ?
				prodVariables :
			NODE_ENV === "test" ?
				testVariables :
				devVariables
}

@NgModule({
	providers: [
		{
			provide: ENV,
			// useFactory instead of useValue so we can easily add more logic as needed.
			useFactory: environmentFactory
		},
	]
})
export class EnvironmentsModule {}