import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController } from "ionic-angular";
import { AjudaProvider } from "../../providers/ajuda";

/**
 * Generated class for the Ajuda page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-ajuda",
	templateUrl: "ajuda.html",
	providers: [AjudaProvider]
})
export class AjudaPage {

	ajudas: any
	loading: any

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private loadingCtrl: LoadingController,
		private ajp: AjudaProvider
	) {
		this.showLoader();
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad Ajuda");
		this.init();
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		});
		this.loading.present();
	}

	init() {
		this.ajp.getAll({}, {sort: {"ordem": 1} }).then(ajudas => {
			this.ajudas = ajudas;
			this.ajudas.forEach(aj => {aj.collapse = false});
			this.loading.dismiss();
		});
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}

}
