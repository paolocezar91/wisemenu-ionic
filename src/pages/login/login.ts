import { Component } from "@angular/core"
import { IonicPage, NavController, LoadingController, AlertController } from "ionic-angular"
import { AuthProvider } from "../../providers/auth"

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-login",
	templateUrl: "login.html",
})
export class LoginPage {

	user: any
	errorMessage: any = ""
	loading: any

	constructor(
		public navCtrl: NavController,
		private ap: AuthProvider,
		private loadingCtrl: LoadingController,
		private alertCtrl: AlertController,
	) {
		this.user = {}
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad Login")
	}

	login() {
		this.showLoader()

		const credentials = {
			email: this.user.email,
			password: this.user.password
		}

		this.ap.login(credentials).then((result) => {
			this.loading.dismiss()
			this.navCtrl.setRoot("HomePage")
		}, (err) => {
			console.log("err", err)
			this.loading.dismiss()

			this.errorMessage = "Houve problemas de comunicação com o servidor. Tente mais tarde."
			this.showError(this.errorMessage)
		})

	}

	goRegister() {
		this.navCtrl.push("RegisterPage")
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})
		this.loading.present()
	}

	showError(errorMessage) {
		const alert = this.alertCtrl.create({
				title: "Erro",
				message: errorMessage,
				buttons: [
					{text: "Ok"}
				]
			})

		alert.present()
	}
}
