import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AdminTagsPage } from "./admin-tags";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
	declarations: [
		AdminTagsPage,
	],
	imports: [
		IonicPageModule.forChild(AdminTagsPage),
		ComponentsModule
	],
})
export class AdminTagsPageModule {}
