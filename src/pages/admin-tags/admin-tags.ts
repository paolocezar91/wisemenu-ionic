import { Component } from "@angular/core"
import { IonicPage, NavController, NavParams, LoadingController, Events } from "ionic-angular"
import { AuthProvider } from "../../providers/auth"
import { TagsProvider } from "../../providers/tags"
/**
 * Generated class for the AdminTags page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-admin-tags",
	templateUrl: "admin-tags.html",
	providers: [TagsProvider]
})
export class AdminTagsPage {

	items: any
	// este objeto direciona a criação da table-admin com os parametros corretos
	options: any = {
		cols: [
			{
				label: "Título",
				attr: "titulo"
			},
			{
				label: "Tipo",
				attr: "tipo"
			}
		],
		page: 1,
		limit: 25,
		buttons: [
			{
				label: "Novo",
				color: "primary",
				icon: "fa-plus",
				callback: () => { this.pushAdminTagPage({_id: "new"}) }
			}
		]
	}
	loading: any

	constructor(
		private navCtrl: NavController,
		private ap: AuthProvider,
		private tp: TagsProvider,
		private loadingCtrl: LoadingController,
		private events: Events
	) {
	}

	ionViewCanEnter() {
		return new Promise((resolve, reject) => {
			// Checagem de role do usuário
			this.ap.currentUserRoleCheck("admin").then((res) => {
				if (!res) {
					reject("Não autorizado")
				} else {
					resolve(true)
				}
			})
		}).catch(err => this.navCtrl.setRoot("HomePage"))
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad AdminTags");
		this.getTags(this.options.page, this.options.limit)
	}

	getTags(page, limit) {
		try {
			const data = {}, options = {"page": page, "limit": limit, select: "titulo tipo", sort: "tipo titulo"}

			this.showLoader()
			this.tp.getAll(data, options).then(receitas => {
				this.items = receitas
				this.loading.dismiss()
			})
		} catch (err) {
			console.log(err)
			this.loading.dismiss()
		}
	}

	setOptions(options) {
		this.options = options
		this.getTags(this.options.page, this.options.limit)
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})

		this.loading.present()
	}

	// importExcel() {
	// 	this.fileInput.nativeElement.click()
	// }

	// uploadXls(fileInput: any) {
	// 	this.filesToUpload = <Array<File>> fileInput.target.files

	// 	const params = {
	// 		attr: fileInput.target.name,
	// 	}

	// 	this.showLoader()

	// 	this.rp.uploadXls(params, this.filesToUpload).then(result => {
	// 		this.loading.dismiss()
	// 		this.getTags(this.options.page, this.options.limit)
	// 	}, (err) => {
	// 		this.loading.dismiss()
	// 	})
	// }

	// removeItem(event) {
	// 	const data = {_id: event._id}, options = {"page": this.options.page, "limit": this.options.limit, select: "titulo"}

	// 	this.showLoader()
	// 	// Recarrega receitas
	// 	this.rp.removeReceita(data, options).then(receitas => {
	// 		this.items = receitas
	// 		this.loading.dismiss()
	// 	})
	// }

	pushAdminTagPage(item) {
		this.events.subscribe("reloadPage", () => {
			this.getTags(this.options.page, this.options.limit)
		})
		this.navCtrl.push("AdminTagPage", {_id: item._id})
	}

	rootAdminHome() {
		this.navCtrl.setRoot("AdminHomePage")
	}

}
