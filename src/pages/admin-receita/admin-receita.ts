import { Component, Inject } from "@angular/core"
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, Events } from "ionic-angular"
import { AuthProvider } from "../../providers/auth"
import { FormBuilder, FormGroup, Validators } from "@angular/forms"
import { ReceitasProvider } from "../../providers/receitas"
import { TagsProvider } from "../../providers/tags"
import { ENV } from "../../env/environment-variables.token"
import { FilterArrayPipe } from "../../pipes/filter-array"
import _ from "lodash"

/**
 * Generated class for the AdminReceita page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
	segment: "admin-receita/:_id"
})
@Component({
	selector: "page-admin-receita",
	templateUrl: "admin-receita.html",
	providers: [ReceitasProvider, TagsProvider, FilterArrayPipe]
})
export class AdminReceita {

	filesToUpload: any

	receita: any
	receitaClone: any
	loading: any
	tipo_tags: any

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public rp: ReceitasProvider,
		private tp: TagsProvider,
		private ap: AuthProvider,
		public formBuilder: FormBuilder,
		private loadingCtrl: LoadingController,
		private alertCtrl: AlertController,
		private toastCtrl: ToastController,
		private fap: FilterArrayPipe,
		private events: Events,
		@Inject(ENV) public ENV
	) {
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad AdminReceita")
		this.init()
	}

	ionViewCanEnter() {
		return new Promise((resolve, reject) => {
			// Checagem de role do usuário
			this.ap.currentUserRoleCheck("admin").then((res) => {
				if (!res) {
					reject("Não autorizado")
				} else {
					resolve(true)
				}
			})
		}).catch(err => this.navCtrl.setRoot("HomePage"))
	}

	ionViewWillLeave() {
		if (this.isDifferent()) {
			const alert = this.alertCtrl.create({
				title: "Receita alterada",
				message: "Deseja salvar alterações?",
				buttons: [
					{
						text: "Não",
						handler: data => {}
					},
					{
						text: "Sim",
						handler: data => {
							this.createOrUpdateReceita()
						}
					}
				]
			})
			alert.present()
		}
		this.events.unsubscribe("reloadPage")
	}

	init() {
		try {
			this.getReceita()
			this.getTags()
		} catch (err) {
			console.log(err)
		}
	}

	getReceita() {
		if (this.navParams.get("_id") !== "new") {
			const data = {_id: this.navParams.get("_id")}, options = {populate: {path: "tags"}, sort: {tags: 1}}
			this.showLoader()
			this.rp.getReceita(data, options).then(receita => {
				this.receita = receita
				this.receitaClone = this.clone(this.receita)
				console.log(this.receitaClone);
				console.log(this.receita);
				this.loading.dismiss()
			}).catch(err => {
				this.loading.dismiss()
			})
		} else {
			this.receita = {
				titulo: "",
				icone: "",
				modo_de_preparo: "",
				ingredientes: [],
				informacoes_nutricionais: [],
				tags: [],
				favoritos: []
			}
			this.receitaClone = this.clone(this.receita)
		}
	}

	createOrUpdateReceita() {
		this.showLoader()
		this.rp.createOrUpdateReceita(this.receita, {}).then(receita => {
			// if its a create, should reload on back to previous page
			if (!receita["ok"]) {
				this.events.publish("reloadPage")
				this.receita = receita
				this.receitaClone = this.clone(this.receita)
			}
			this.showToast("As alterações foram salvas")
			this.loading.dismiss()
		})
	}

	// Recebe as tags e o tipo e retorna o objeto daquele tipo
	filterTipoTags(tags, tipo) {
		return tags.filter((item) => {
			return item.tipo === tipo
		})[0]
	}

	getTags() {
		const options = {
			aggregate: true,
			match: [
				"filho",
				"normal",
				"nacionalidade",
				"refeicao",
				"tempo_de_preparo",
				"porcao_sugerida",
				"tipo_de_preparo",
				"dificuldade",
				"tipo_de_prato",
				"avaliacoes"
			]
		}

		this.tp.getAll({}, options).then(tipo_tags => {
			this.tipo_tags = []
			_.each(options.match, (tag) => {
				this.tipo_tags.push(
					{
						tipo: this.filterTipoTags(tipo_tags, tag).tipo,
						tags: this.filterTipoTags(tipo_tags, tag).tags
							.sort((a, b) => {
									if (a.titulo < b.titulo) return -1
									if (a.titulo > b.titulo) return 1
									return 0
								}
							)
					}
				)
			})
		})
	}

	adicionarIngrediente() {
		this.receita.ingredientes.push({
			ingrediente: "",
			porcao: "",
			unidade: ""
		})
	}

	removerIngrediente(idx) {
		this.receita.ingredientes.splice(idx, 1)
	}

	uploadImagem(fileInput: any) {
		this.filesToUpload = <Array<File>> fileInput.target.files

		const params = {
			attr: fileInput.target.name,
		}

		this.showLoader()

		this.rp.uploadImg(params, this.filesToUpload).then(result => {
			this.receita.icone = result
			this.loading.dismiss()
		}, (error) => {
			this.loading.dismiss()

			error = JSON.parse(error)
			const alert = this.alertCtrl.create({
				title: "Erro!",
				message: error.err,
				buttons: ["OK"]
			})
			alert.present()
		})
	}

	filterTipo(tags, campo, tipo) {
		return this.fap.transform(tags, campo, tipo)
	}

	filterTags(tags, tagsEscolhidas) {
		return _.filter(tags, function(obj){ return !_.find(tagsEscolhidas, obj) })
	}

	adicionarTag(tagsEscolhidas, r) {
		tagsEscolhidas.push(r)
		tagsEscolhidas.sort((a, b) => {
			return (a.id > b.id ? 1 : 0)
		})
	}

	removerTag(event) {
		this.receita.tags = this.receita.tags.filter(i => i._id !== event.t._id)
	}

	rootAdminHome() {
		this.navCtrl.setRoot("AdminHomePage")
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})

		this.loading.present()
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		})
		toast.present()
	}

	clone(original) {
		return JSON.parse(JSON.stringify(original))
	}

	isDifferent() {
		return !_.isEqual(this.receita, this.receitaClone)
	}

}
