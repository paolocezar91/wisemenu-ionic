import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AdminReceita } from "./admin-receita";
import { ComponentsModule } from "../../components/components.module";
import "froala-editor/js/froala_editor.pkgd.min.js";
import { FroalaEditorModule, FroalaViewModule } from "angular-froala-wysiwyg";

@NgModule({
	declarations: [
		AdminReceita,
	],
	imports: [
		IonicPageModule.forChild(AdminReceita),
		FroalaEditorModule.forRoot(),
		FroalaViewModule.forRoot(),
		ComponentsModule
	],
	exports: [
		AdminReceita
	]
})
export class AdminReceitaModule {}
