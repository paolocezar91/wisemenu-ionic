import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AvaliacoesPage } from "./avaliacoes";

@NgModule({
	declarations: [
		AvaliacoesPage,
	],
	imports: [
		IonicPageModule.forChild(AvaliacoesPage),
	],
	entryComponents: [
		AvaliacoesPage
	]
})
export class AvaliacoesModule {}
