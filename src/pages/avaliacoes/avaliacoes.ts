import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController } from "ionic-angular";
import { FavoritosProvider } from "../../providers/favoritos"
import { AuthProvider } from "../../providers/auth";

/**
 * Generated class for the Avaliacoes page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-avaliacoes",
	templateUrl: "avaliacoes.html",
	providers: [FavoritosProvider]
})
export class AvaliacoesPage {

	segment: any = "receitas"
	receitas: any

	loading: any

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public favp: FavoritosProvider,
		private ap: AuthProvider,
		private loadingCtrl: LoadingController

	) {
		this.showLoader();
	}

	ionViewDidLoad() {
		console.log("Hello Filtros Page");
		this.init();
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		});
		this.loading.present();
	}

	init() {
		try {
			if (this.ap.currentUserRoleCheck("guest")) {
				this.loading.dismiss();
			} else {
				const data = {user_id: this.ap.getCurrentUser()._id, active: true}
				const options = {select: "receita_id", populate: {path: "receita_id", select: "titulo"}}

				this.favp.getAll(data, options).then(receitas => {
					this.receitas = receitas;
					console.log("this.receitas", this.receitas);
					this.loading.dismiss();
				});
			}
		} catch (err) {
			console.log(err);
			this.loading.dismiss();
		}
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}

}
