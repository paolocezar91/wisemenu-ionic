import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth";
import { Device } from "@ionic-native/device";

/**
 * Generated class for the Register page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-register",
	templateUrl: "register.html",
	providers: [Device]
})
export class RegisterPage {

	user: any
	errorMessage: any = "";
	loading: any;

	registerForm: FormGroup
	registerSubmitAttempt: Boolean = false;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		private alertCtrl: AlertController,
		private ap: AuthProvider,
		public formBuilder: FormBuilder,
		private device: Device,
	) {
		this.registerForm = this.formBuilder.group({
			name: ["", Validators.compose([ Validators.maxLength(30), Validators.pattern("[a-zA-Z ]*"), Validators.required ])],
			email: ["", Validators.compose([ Validators.email , Validators.required ])],
			password: ["", Validators.compose([ Validators.required])],
			termos: [true,  Validators.compose([ Validators.requiredTrue ])]
		});
	}

	ionViewDidLoad() {
		console.log("Hello Register Page");
		this.init();
	}

	init() {

	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}


	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Atualizando..."
		});
		this.loading.present();
	}

	showError(errorMessage) {
		const alert = this.alertCtrl.create({
				title: "Erro",
				message: errorMessage,
				buttons: [
					{text: "Ok"}
				]
			});

		alert.present();
	}

	register(registerForm) {
		this.showLoader();

		this.errorMessage = "";

		// Se há erros de validação do formulário
		if (!registerForm.valid) {
			if (registerForm.controls.name.errors) {
				this.errorMessage += "Você deve digitar seu nome. <br>"
			}
			if (registerForm.controls.email.errors) {
				this.errorMessage += "Você deve digitar um e-mail válido. <br>"
			}
			if (registerForm.controls.password.errors) {
				this.errorMessage += "Você deve digitar uma senha. <br>"
			}
			if (registerForm.controls.termos.errors) {
				this.errorMessage += "Você deve aceitar os termos de uso. <br>"
			}

			this.loading.dismiss();
			this.showError(this.errorMessage);
		} else {
			// Validação ok, gera os dados para enviar ao servidor
			const credentials = {
				name: registerForm.value.name,
				email: registerForm.value.email,
				password: registerForm.value.password,
				termos: registerForm.value.termos,
				uuid: this.device.uuid
			};

			this.ap.createAccount(credentials).then((result) => {
				this.loading.dismiss();
				this.navCtrl.setRoot("HomePage");
			}, (err) => {
				console.log("err", err)
				this.loading.dismiss();
				if (err.status === 0) {
					this.errorMessage = "Houve problemas de comunicação com o servidor. Tente mais tarde."
				} else {
					this.errorMessage = JSON.parse(err._body).error;
				}
				this.showError(this.errorMessage);
			});
		}
	}

	pop() {
		if (this.navCtrl.canGoBack()) {
			this.navCtrl.pop();
		} else {
			this.navCtrl.setRoot("HomePage")
		}
	}

}
