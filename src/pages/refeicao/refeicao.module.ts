import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { RefeicaoPage } from "./refeicao";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
	declarations: [
		RefeicaoPage
	],
	imports: [
		IonicPageModule.forChild(RefeicaoPage),
		ComponentsModule,
	],
	entryComponents: [
		RefeicaoPage
	]
})
export class RefeicaoModule {}
