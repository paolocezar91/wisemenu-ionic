import { Component, ViewChild } from "@angular/core"
import { IonicPage, NavController, AlertController, ModalController, NavParams, LoadingController, ToastController, Content } from "ionic-angular"
import { ReceitasProvider } from "../../providers/receitas"
import { FavoritosProvider } from "../../providers/favoritos"
import { ListasProvider } from "../../providers/listas"
import { CardapiosProvider } from "../../providers/cardapios"
import { RefeicoesProvider } from "../../providers/refeicoes"
import { AuthProvider } from "../../providers/auth"
import { CurrentCardapiosProvider } from "../../providers/current-cardapios"
import _ from "lodash"

/*
	Generated class for the Refeicao page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-refeicao",
	templateUrl: "refeicao.html",
	providers: [CardapiosProvider, RefeicoesProvider, ReceitasProvider, FavoritosProvider, ListasProvider]
})

export class RefeicaoPage {

	@ViewChild(Content) content: Content

	autosave: any = true
	refeicao: any
	refeicoes: any
	receitas: any
	cardapio: any
	cardapioIndex: any
	modelCardapioDia: any
	modelCardapioDiaClone: any
	refeicaoIndex: any

	searchTerm: any

	loading: any


	constructor(
		private nav: NavController,
		private navParams: NavParams,
		private alertCtrl: AlertController,
		private modalCtrl: ModalController,
		private ccp: CurrentCardapiosProvider,
		private recP: ReceitasProvider,
		private rp: RefeicoesProvider,
		private lp: ListasProvider,
		private ap: AuthProvider,
		private favsp: FavoritosProvider,
		private loadingCtrl: LoadingController,
		private toastCtrl: ToastController,
	) {
		this.showLoader()
	}

	ionViewDidLoad() {
		console.log("Hello Refeicao Page")
		this.init()
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})
		this.loading.present()
	}

	init() {
		try {
			const cardapioParams = this.navParams.get("c")

			this.cardapio = this.ccp.getCardapio()
			this.cardapioIndex = this.ccp.getCardapioIndex()
			this.modelCardapioDia = this.ccp.getModelCardapioDia()
			this.modelCardapioDiaClone = this.clone(this.modelCardapioDia)

			this.refeicao = this.navParams.get("refeicao")
			this.refeicoes = this.navParams.get("refeicoes")

			this.refeicaoIndex = this.ccp.getRefeicaoIndex()

			this.getReceitas()
		} catch (err) {
			console.log(err)
			this.loading.dismiss()
		}
	}


	adicionarReceita(r) {
		this.modelCardapioDia = this.ccp.getModelCardapioDia()
		this.modelCardapioDia.refeicoes[this.refeicaoIndex].touched = true
		this.modelCardapioDia.refeicoes[this.refeicaoIndex].receitas.push( this.clone(r))
		this.showToast("Receita adicionada")
	}

	getReceitas(ev?: any) {
		const data: any = {user_id: this.ap.getCurrentUser()._id, tag_refeicao: this.refeicao.tag}

		if (ev) {
			const val = ev.target.value
			if (val && val.trim() !== "") {
				data.search = val
			}
		}
		try {
			this.recP.getReceitasFiltered(data, {}).then((receitas) => {
				this.receitas = receitas

				const parentes = []
				const filhos = []
				const orderType = "DESC"
				const currentField = "score"

				// Separa quais tags são tipo "parente" e tipo "filho"
				for (let r, i = this.receitas.length - 1; i >= 0; i--) {
					r = this.receitas[i]
					if (r.tags.tipo === "parente") { parentes.push( {i, _id: r.tags._id} ) } // Nas tags parente, guarda o índice que a tag está no array de receitas, e o _id para comparar com o parent_id
					if (r.tags.tipo === "filho") { filhos.push(r) } // Nas tags filho, guarda o parent_id e ele
					r.receitas = this.orderBy(r.receitas, currentField, orderType)
				}

				// Construindo a hierarquia de pai e filho

				// Para cada parente
				for (let p, j = parentes.length - 1; j >= 0; j--) {
					p = parentes[j]

					// Seta receitas deste parente como um array vazio
					this.receitas[p.i].receitas = []
					// Para cada filho
					for (let f, k = filhos.length - 1; k >= 0; k--) {
						f = filhos[k]

						// Se f for filho de p, adiciona na array de receitas
						if (f.tags.parent_id === p._id) {
							this.receitas[p.i].receitas.push(f)
							filhos.splice(k, 1)
						}
					}

					this.receitas[p.i].receitas = this.orderBy(this.receitas[p.i].receitas, currentField, orderType)
					this.receitas[p.i].receitas.sort(function(a, b) {return a.tags.titulo > b.tags.titulo})
				}

				this.receitas = this.receitas.filter((r) => {
					if (r.tags.tipo === "filho") {
						return false
					}
					if (r.tags.tipo === "refeicao") {
						return false
					}
					return true
				})

				// var orderType = "DESC"
				// var currentField = "receitas"

				// this.receitas.sort(function(a,b) {
				// 	if (a[currentField].length < b[currentField].length) return 1
				// 	if (a[currentField].length > b[currentField].length) return -1
				// 	return 0
				// })

				this.loading.dismiss()

			})
		} catch (err) {
			console.error(err)
			this.loading.dismiss()
		}
	}

	orderBy(ar, currentField, orderType) {
		return ar.sort(function(a, b) {
				if (orderType === "ASC") {
						if (a[currentField] < b[currentField]) return -1
						if (a[currentField] > b[currentField]) return 1
						return 0
				} else {
						if (a[currentField] < b[currentField]) return 1
						if (a[currentField] > b[currentField]) return -1
						return 0
				}
		})
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		})
		toast.present()
	}

	popView() {
		this.nav.pop()
	}

	rootHome() {
		this.nav.setRoot("HomePage")
	}

	receitaModal(r) {
		const modal = this.modalCtrl.create("ReceitaPage", {
			receita: r,
		})
		modal.onDidDismiss(adicionar => {
			if (adicionar) {
				this.adicionarReceita(r)
			}
		})
		modal.present()
	}

	updateFav(re) {
		this.showLoader()

		const _favorito = {
			user_id: this.ap.getCurrentUser()._id,
			receita_id: re._id,
			active: !re.favorito
		}
		re.favorito = !re.favorito

		this.favsp.update(_favorito, {}).then(favorito => {
			const favoritos = this.receitas.find(function(r) {
				return r.tags.tipo === "favoritos"
			})
			if (re.favorito) {
				favoritos.receitas.push(re)
				this.showToast("Receita adicionada aos favoritos")
			} else {
				favoritos.receitas = favoritos.receitas.filter(
					function(r) {
						return r._id !== re._id
					}
				)
				this.showToast("Receita removida dos favoritos")
			}


			this.loading.dismiss()
		})
	}

	alterarRefeicao(event) {
		if ((event.r.ordem + event.i) >= this.refeicoes.length) {
			this.refeicaoIndex = this.ccp.setRefeicaoIndex(0)
		} else if ((event.r.ordem + event.i) < 0) {
			this.refeicaoIndex = this.ccp.setRefeicaoIndex( this.refeicoes.length - 1 )
		} else {
			this.refeicaoIndex = this.ccp.setRefeicaoIndex(event.r.ordem + event.i)
		}
		this.refeicao = this.refeicoes[this.refeicaoIndex]
		this.getReceitas()
	}

	copiarDia(dia) {
		if (this.cardapio.dias.length !== 1) {
			if ((this.ccp.getCardapioIndex() + dia) >= this.cardapio.dias.length ) {
				this.cardapioIndex = this.ccp.setCardapioIndex(0)
			} else if ((this.ccp.getCardapioIndex() + dia) < 0) {
				this.cardapioIndex = this.ccp.setCardapioIndex( this.cardapio.dias.length - 1 )
			} else {
				this.cardapioIndex = this.ccp.setCardapioIndex(this.ccp.getCardapioIndex() + dia)
			}

			this.modelCardapioDia = this.ccp.setModelCardapioDia(this.cardapio.dias[this.cardapioIndex])

			// Resetar collapse de tags de receitas
			this.resetCollapse()
		}
	}

	resetCollapse() {
		for (let r, i = 0; i < this.receitas.length; ++i) {
			r = this.receitas[i]
			if (r.collapse) {
				r.collapse = !r.collapse
			}
			if (r.receitas) {
				for (let rr, j = 0; j < r.receitas.length; ++j) {
					rr = r.receitas[j]
					if (rr.collapse) {
						rr.collapse = !rr.collapse
					}
				}
			}
		}
	}

	scrollTo(scrollHeight) {
		setTimeout(() => {
			this.content.scrollTo(0, scrollHeight)
		}, 500)
	}

	clone(original) {
		return JSON.parse(JSON.stringify(original))
	}

	isDifferent() {
		return !_.isEqual(this.modelCardapioDia, this.modelCardapioDiaClone)
	}


}
