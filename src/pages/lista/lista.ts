import { Component } from "@angular/core";
import { IonicPage, NavController, AlertController, ModalController, NavParams, LoadingController, ToastController } from "ionic-angular";
import { ListasProvider } from "../../providers/listas";
import { AuthProvider } from "../../providers/auth";
import { UtilsProvider } from "../../providers/utils";
import waterfall from "async-es/waterfall";
import { orderBy, deburr } from "lodash"

/*
	Generated class for the Lista page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-lista",
	templateUrl: "lista.html",
	providers: [ListasProvider]
})
export class ListaPage {

	lista: any
	listaClone: any
	callback: any
	loading: any

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private lp: ListasProvider,
		private ap: AuthProvider,
		private up: UtilsProvider,
		private alertCtrl: AlertController,
		private modalCtrl: ModalController,
		private toastCtrl: ToastController,
		private loadingCtrl: LoadingController
	) {
		this.showLoader();
	}

	ionViewDidLoad() {
		console.log("Hello Lista Page");
		this.init();
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando...",
		});
		this.loading.present();
	}

	init() {
		try {
			const listaParams = this.navParams.get("l");
			waterfall(
				[
					(cb) => {
						// Se o usuário for visitante
						if (this.ap.currentUserRoleCheck("guest")) {
							// E se for uma lista nova
							if (listaParams._id === undefined ) {
								// Query pra contar o número de cardápios do usuário
								const data = {user_id: this.ap.getCurrentUser()._id}
								const options = {count: true}
								this.lp.getAll(data, options).then(count => {
									// Se o número de cardápios for maior do que 0, ele deve ser redirecionado para tela de registro, e depois redirecionando para tela de cardápio
									if (count > 0) {
										cb(true);
									} else {
										cb(false);
									}
								});
							} else {
								cb(false);
							}
						} else {
							cb(false);
						}
					},
				],
				(needRegister) => {
					if (needRegister) {
						const registerAlert = this.alertCtrl.create({
							enableBackdropDismiss: false,
							title: "Registrar",
							message: "Para criar mais de uma lista você precisa se registrar.",
							buttons: [
								{
									text: "Voltar",
									handler: data => {
										this.loading.dismiss();
										if (this.navCtrl.canGoBack()) {
											this.navCtrl.pop();
										} else {
											this.navCtrl.setRoot("HomePage");
										}
									}
								},
								{
									text: "Se registrar",
									handler: data => {
										this.loading.dismiss();
										this.navCtrl.setRoot("RegisterPage", {nextPage: "ListaPage"});
									}
								}
							]
						})
						registerAlert.present();
					} else {
						this.callback = this.navParams.get("updateListaCB");
						if (listaParams._id === undefined ) {
							this.lista = listaParams;
							this.listaClone = this.up.clone(this.lista);
							this.loading.dismiss();
						} else {
							const data = {_id: listaParams._id}
							const options = { populate: [{field: "cardapio_id", select: "titulo"}, {field: "itens.produto_id", select: "titulo"}] };

							this.lp.getOne(data, options).then(lista => {
								this.lista = lista;

								this.lista.itens.forEach(item => {
									if (item.check === undefined) {
										item.check = false;
									}
								});

								this.reorderItensLista().then((res) => {
									this.listaClone = this.up.clone(this.lista);
									console.log(this.listaClone, this.lista)
									this.loading.dismiss();
								});

							});
						}
					}
				}
			);
		} catch (err) {
			console.log(err);
			this.loading.dismiss();
		}
	}

	adicionarValorItem(it) {
		const alert = this.alertCtrl.create({
			title: "Valor do produto",
			message: "Informar o valor de " + it.produto_id.titulo + ".",
			inputs: [
				{
					name: "valor",
					placeholder: it.valor,
					type: "number"
				},
			],
			buttons: [
				{
					text: "Apagar",
					handler: data => {
						delete it.valor;
						this.showToast("Valor do item apagado");
					}
				},
				{text: "Cancelar"},
				{
					text: "OK",
					handler: data => {
						it.valor = Number(data.valor);
						this.showToast("Valor do item alterado");
					}
				}
			]
		});
		alert.present();
	}

	editarValorTotal() {
		const alert = this.alertCtrl.create({
			title: "Valor da lista",
			message: "Informe o valor total da lista.",
			inputs: [
				{
					name: "valorTotal",
					placeholder: this.valorLista(),
					type: "number"
				},
			],
			buttons: [
				{
					text: "Apagar",
					handler: data => {
						delete this.lista.valorTotal;
						this.showToast("Valor da lista apagado");
					}
				},
				{text: "Cancelar"},
				{
					text: "OK",
					handler: data => {
						this.lista.valorTotal = data.valorTotal
						this.showToast("Valor da lista alterado");
					}
				}
			]
		});
		alert.present();
	}

	removerItem(it) {
		const alert = this.alertCtrl.create({
				title: "Remover item",
				message: "Remover <strong>" + it.produto_id.titulo + "</strong>?",
				buttons: [
					{text: "Não"},
					{
						text: "Sim",
						handler: data => {
							this.lista.itens = this.lista.itens.filter(i => i !== it);
							this.showToast("Item removido");
						}
					}
				]
			});
		alert.present();
	}

	adicionarItem() {
		const modal = this.modalCtrl.create("ListaItemPage", {item: {produto_id: {}}});
		modal.onDidDismiss(data => {
			if (data) {
				this.lista.itens.push(data);
				this.showToast("Item adicionado à lista");
				this.reorderItensLista();
			}
		});
		modal.present();

	}

	editarItem(it, i) {
		const modal = this.modalCtrl.create("ListaItemPage", {item: it});
		modal.onDidDismiss(item => {
			if (item) {
				this.lista.itens[i] = item;
				this.showToast("Item alterado");
				this.reorderItensLista();
			}
		});
		modal.present();

	}

	reorderItensLista() {
		return new Promise(resolve => {
			setTimeout(() => {
				this.lista.itens = orderBy(this.lista.itens,
					[
						function(it){return it["check"] || false },
						function(it){return deburr(it["produto_id"]["titulo"]) }
					],
					["asc", "asc"]
				);
				resolve(true);
			}, 500);
		});
	}

	valorLista(): string {
		if (this.lista) {
			if (this.lista.valorTotal) {
				return this.lista.valorTotal.toString();
			} else {
				let soma = 0;
				for (const it of this.lista.itens){
					if (it.valor !== undefined) {
						soma = Number(soma) + Number(it.valor);
					}
				}
				return soma.toString();
			}
		}

	}

	saveLista() {
		this.updateLista(false).then((l: any) => {
			if (l.upserted) {
				const data = { push: {_id: l.upserted[0]._id, titulo: this.lista.titulo} };
				this.callback(data).then(() => {});
			} else {
				const data = { update: {_id: this.lista._id, titulo: this.lista.titulo} };
				this.callback(data).then(() => {});
			}
			this.listaClone = this.up.clone(this.lista);
			this.loading.dismiss();
		});
	}

	editListaDetalhesModal() {
		const modal = this.modalCtrl.create("ListaDetalhesPage", {lista: this.lista});
		modal.onDidDismiss(data => {});
		modal.present();
	}

	updateLista(dismiss) {
		this.showLoader();
		return new Promise(resolve => {
			this.lp.update(this.lista, {}).then(lista => {
				if (dismiss) {
					this.loading.dismiss();
				}
				this.showToast("As alterações foram salvas");
				resolve(lista);
			});
		});
	}

	ionViewWillLeave() {
		if (this.up.isDifferent(this.lista, this.listaClone)) {
			const alert = this.alertCtrl.create({
				title: "Lista alterada",
				message: "Deseja salvar alterações?",
				buttons: [
					{
						text: "Não",
					},
					{
						text: "Sim",
						handler: data => {
							this.updateLista(true).then((l: any) => {
								// Callback para atualizar a tela listas com os dados da nova lista
								if (l.upserted) {
									const data = { push: {_id: l.upserted[0]._id, titulo: this.lista.titulo} };
									this.callback(data).then(() => {});
								} else {
									const data = { update: {_id: this.lista._id, titulo: this.lista.titulo} };
									this.callback(data).then(() => {});
								}
							});
						}
					}
				]
			});
			alert.present();
		}
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		});

		toast.present();
	}


	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}

}
