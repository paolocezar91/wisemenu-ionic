import { Component } from "@angular/core"
import { IonicPage, NavController, ModalController, AlertController, LoadingController, ToastController } from "ionic-angular"
import { ListasProvider } from "../../providers/listas"
import { AuthProvider } from "../../providers/auth"
import _ from "lodash"


/*
	Generated class for the Listas page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-listas",
	templateUrl: "listas.html",
	providers: [ListasProvider]
})
export class ListasPage {

	listas: any
	guest: any = false
	_: any = _

	loading: any

	constructor(
		public navCtrl: NavController,
		public alertCtrl: AlertController,
		private lp: ListasProvider,
		private ap: AuthProvider,
		private modalCtrl: ModalController,
		private loadingCtrl: LoadingController,
		private toastCtrl: ToastController
	) {
		this.showLoader()
	}

	ionViewDidLoad() {
		console.log("Hello Listas Page")
		this.init()
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})
		this.loading.present()
	}

	init() {
		try {
			const data = {user_id: this.ap.getCurrentUser()._id}
			const options = {select: "titulo createdAt cardapio_id", populate: {field: "cardapio_id", select: "titulo"}}

			this.lp.getAll(data, options).then(listas => {
				this.listas = listas
				this.loading.dismiss()
			})
		} catch (err) {
			console.log(err)
			this.loading.dismiss()
		}
	}

	pushListaPage(l) {
		const updateListaCB = (_params) => {
			return new Promise((resolve, reject) => {
				if (_params.push) {
					this.listas.push(_params.push)
				} else if (_params.update) {
					const find = this.listas.find(l => { return l._id === _params.update._id })
					find.titulo = _params.update.titulo
				}

				resolve()
			})
		}
		this.navCtrl.push("ListaPage", {l, updateListaCB})
	}

	adicionarLista() {
		const novaLista = {
			itens: [],
			titulo: "Lista nova",
			user_id: this.ap.getCurrentUser()._id
		}
		this.pushListaPage(novaLista)
	}

	removeLista(l) {
		const alert = this.alertCtrl.create({
				title: "Remover lista?",
				message: "Remover <strong>" + l.titulo + "</strong>?",
				buttons: [
					{text: "Não"},
					{
						text: "Sim",
						handler: data => {
							this.lp.delete({user_id: this.ap.getCurrentUser()._id, _id: l._id}).then(resolve => {
								this.listas = this.listas.filter(i => i !== l)
								this.showToast("Lista removida")
							})

						}
					}
				]
			})
		alert.present()
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		})
		toast.present()
	}

	editListaDetalhesModal(l) {
		const modal = this.modalCtrl.create("ListaDetalhesPage", {lista: l})
		modal.onDidDismiss(data => {})
		modal.present()
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}

	registerPage() {
		this.navCtrl.setRoot("RegisterPage", {nextPage: "CardapiosPage"})
	}

	isGuest() {
		this.ap.currentUserRoleCheck("guest")
			.then(res => {
				this.guest = res
			})
			.catch(err => {
				this.guest = false
			})
	}

}
