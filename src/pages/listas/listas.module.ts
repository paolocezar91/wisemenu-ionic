import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ListasPage } from "./listas";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
	declarations: [
		ListasPage,
	],
	imports: [
		IonicPageModule.forChild(ListasPage),
		PipesModule
	],
	entryComponents: [
		ListasPage
	]
})
export class ListasModule {}
