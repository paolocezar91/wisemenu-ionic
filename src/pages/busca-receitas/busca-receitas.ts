import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ReceitasProvider } from "../../providers/receitas";
import { TagsProvider } from "../../providers/tags";

/**
 * Generated class for the BuscaReceitasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-busca-receitas',
  templateUrl: 'busca-receitas.html',
  providers: [ReceitasProvider, TagsProvider]
})
export class BuscaReceitasPage {
	title = "";
	tipo = "";
	information = [];
  filesToUpload: any;
  loading: any;
  tagsIds = [];
  buscaTagsIds = [];
  buscaReceitas = [];

  @ViewChild("fileInput") fileInput: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  	private http: Http, public rp: ReceitasProvider, public tp: TagsProvider, 
    private loadingCtrl: LoadingController) {

  	this.title = this.navParams.get('title');
  	switch(this.title) {
  		case "nacionalidade":
  			this.tipo = "nacionalidade";
  			break;
  		case "preparo":
  			this.tipo = "tipo_de_preparo";
  			break;
  		case "tempo":
  			this.tipo = "tempo_de_preparo";
  			break;
      case "tipo_de_prato":
        this.tipo = "tipo_de_prato";
        break;
      case "dificuldade":
        this.tipo = "dificuldade";
        break;
      case "avaliacoes":
        this.tipo = "avaliacoes";
        break;  
  		default:
  			this.tipo = "tipo_de_preparo";
  	}

    //this.getReceitas();
    this.showLoader();
    this.getTags(this.tipo).then(tags => {
			this.information = tags.docs;
      for(var i = 0; i < this.information.length; i++) {
        this.tagsIds[i] = this.information[i]._id
      }
      this.loading.dismiss();
			//console.log(tags);
		})
  }

  onInput(ev) {
    try {
      this.showLoader();
      if(ev.target.value != "" && ev.type != "mousedown") {
        this.searchReceitas('titulo', ev.target.value)
          .then(receitas => {
            this.buscaTagsIds = [];
            this.buscaReceitas = receitas;
            for (var i = 0; i < receitas.length; i++) {
              for (var o = 0; o < receitas[i].tags.length; o++) {
                if(this.tagsIds.indexOf(receitas[i].tags[o]) != -1){
                  this.buscaTagsIds.push(receitas[i].tags[o]);
                }
              }
            }
            const data = {_id: {"$in": this.buscaTagsIds}}, options = {"page": 1, "limit": 999999}
            this.tp.getAll(data, options).then(tags => {
              this.information = tags.docs;
              this.loading.dismiss();
            });
          });
      } else {
        this.buscaReceitas = [];
        this.getTags(this.tipo).then(tags => {
          this.information = tags.docs;
          this.loading.dismiss();
        })
      }
      
    } catch (err) {
      console.log(err)
      this.loading.dismiss();
    }
  }

  getReceitas(key, value) {
		try {
			const data = {[key]: value}, options = {"page": 1, "limit": 999999}
      return this.rp.getReceitas(data, options);

		} catch (err) {
			console.log(err)
		}

	}

  searchReceitas(key, value) {
    try {
      const data = {[key]: value, tags: this.tagsIds}
      return this.rp.searchReceitas(data);

    } catch (err) {
      console.log(err)
    }

  }

	getTags(tipo) {
		try {
			const data = {tipo: tipo}, options = {"page": 1, "limit": 999999, select: "titulo"}
			return this.tp.getAll(data, options)

		} catch (err) {
			console.log(err)
		}

	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuscaReceitasPage');
  }

  rootAdminHome() {
		this.navCtrl.setRoot("AdminReceitas_2Page")
  }

  toggleSection(i, id) {
    this.showLoader();
    this.information[i].open = !this.information[i].open;
    if(this.buscaReceitas.length == 0) {
      this.getReceitas('tags', id).then(receitas => {
  			this.information[i].children = receitas.docs;
        this.loading.dismiss();
  		});
    } else {
      var receitasSecao = [];
      for (let i = 0; i < this.buscaReceitas.length; i++) {
        if(this.buscaReceitas[i].tags.indexOf(id) != -1) {
          receitasSecao.push(this.buscaReceitas[i]);
        }
      }
      this.information[i].children = receitasSecao;
      this.loading.dismiss();
    }
  }

  toggleItem(i, j) {
    this.information[i].children[j].open = !this.information[i].children[j].open;
  }

  pushAdminReceitaPage(itemId) {
    this.navCtrl.push("AdminReceita", {_id: itemId});
  }

  removeItem(itemId, j, i) {
    const data = {_id: itemId};
    this.showLoader();
    this.rp.removeReceita(data).then(receitas => {
      this.information[i].children.splice(j, 1);
      this.loading.dismiss();
    })
  }

  uploadXls(fileInput: any) {
    this.filesToUpload = <Array<File>> fileInput.target.files

    const params = {
      attr: fileInput.target.name,
    }
    this.showLoader();
    this.rp.uploadXls(params, this.filesToUpload).then(result => {
      console.log(result);
      this.loading.dismiss();
    });
  }

  importExcel() {
    this.fileInput.nativeElement.click()
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: "Carregando..."
    })

    this.loading.present()
  }

}
