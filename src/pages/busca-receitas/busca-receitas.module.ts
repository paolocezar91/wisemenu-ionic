import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscaReceitasPage } from './busca-receitas';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    BuscaReceitasPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscaReceitasPage),
    PipesModule
  ],
})
export class BuscaReceitasPageModule {}
