import { Component, ViewChild, ElementRef } from "@angular/core"
import { IonicPage, NavController, AlertController, NavParams, ModalController, LoadingController, ToastController, Select } from "ionic-angular"
import { CurrentCardapiosProvider } from "../../providers/current-cardapios"
import { ReceitasProvider } from "../../providers/receitas"
import { CardapiosProvider } from "../../providers/cardapios"
import { RefeicoesProvider } from "../../providers/refeicoes"
import { ListasProvider } from "../../providers/listas"
import { AuthProvider } from "../../providers/auth"
import { UtilsProvider } from "../../providers/utils";
import waterfall from "async-es/waterfall"


/*
	Generated class for the Cardapio page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/

@IonicPage()
@Component({
	selector: "page-cardapio",
	templateUrl: "cardapio.html",
	providers: [CardapiosProvider, RefeicoesProvider, ListasProvider, ReceitasProvider]
})
export class CardapioPage {

	@ViewChild("filtro") filtro: Select
	autosave: any = true
	orderBy: any
	search: any
	searchToggle: any = false
	refeicoes: any
	refeicaoIndex: any
	modelRefeicao: any
	receitas: any
	cardapio: any
	cardapioClone: any
	cardapioIndex: any
	modelCardapioDia: any
	modelCardapioDiaClone: any
	loading: any
	needRegister: any

	constructor(
		public navCtrl: NavController,
		public alertCtrl: AlertController,
		public modalCtrl: ModalController,
		public navParams: NavParams,
		public ccp: CurrentCardapiosProvider,
		private cp: CardapiosProvider,
		private lp: ListasProvider,
		private rp: RefeicoesProvider,
		private recP: ReceitasProvider,
		private ap: AuthProvider,
		private up: UtilsProvider,
		private loadingCtrl: LoadingController,
		private toastCtrl: ToastController
	) {}

	ionViewDidLoad() {
		console.log("ionViewDidLoad Cardapio Page")
		this.init()
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})
		this.loading.present()
	}

	init() {
		// Se o usuário for visitante
		const cardapioParams = this.navParams.get("c")
		waterfall(
			[
				(cb) => {
					this.showLoader()
					this.ap.currentUserRoleCheck("guest")
						.then(res => {
							this.loading.dismiss()
							if (!res) {
								cb(false)
							// Se o usuário for visitante
							} else {
								// E for um cardápio novo
								if (!cardapioParams ) {
									const data = {user_id: this.ap.getCurrentUser()._id}
									const options = {count: true}
									// Query pra contar o número de cardápios do usuário
									this.showLoader()
									this.cp.getAll(data, options).then(count => {
										this.loading.dismiss()
										// Se o número de cardápios for maior do que 0, ele deve ser redirecionado para tela de registro, e depois redirecionando para tela de cardápio
										if (count > 0) {cb(true)}
										else {cb(false)}
									})
								} else {cb(false)}
							}
						})
						.catch(err => cb(false))
				}
			],
			(needRegister) => {
				this.needRegister = needRegister
				if (needRegister) {
					const registerAlert = this.alertCtrl.create({
						enableBackdropDismiss: false,
						title: "Registrar",
						message: "Para prosseguir com a criação de outros cardápios, é necessário o registro de perfil.",
						buttons: [
							{
								text: "Voltar",
								handler: data => {
									this.navCtrl.setRoot("HomePage")
								}
							},
							{
								text: "Registrar",
								handler: data => {
									this.navCtrl.setRoot("RegisterPage", {nextPage: "CardapioPage"})
								}
							}
						]
					})
					registerAlert.present()
				} else {
					// Se o cardapio já existe, já seta os modelos.
					if (cardapioParams) {
						const data = {_id: cardapioParams._id}
						const options = {}

						this.cardapioIndex = this.ccp.setCardapioIndex(0)

						this.showLoader()
						this.cp.getOne(data, options).then(cardapio => {
							this.cardapio = this.ccp.setCardapio(cardapio)
							this.modelCardapioDia = this.ccp.setModelCardapioDia(this.cardapio.dias[this.cardapioIndex])
							this.modelCardapioDiaClone = this.up.clone(this.modelCardapioDia)

							this.refeicoes = this.rp.getRefeicoes({})
							this.refeicaoIndex = this.ccp.setRefeicaoIndex(0)
							this.modelRefeicao = this.refeicoes[this.refeicaoIndex]

							this.cardapioClone = this.up.clone(this.cardapio)
							this.loading.dismiss()
							this.orderByReceitas("normal")
						})
					} else { // Se o cardápio não existe, chama o modal pra preencher os detalhes
						this.cardapioDetalhesModal()
					}

				}
			}
		)
	}

	refeicaoPage(event) {
		this.refeicaoIndex = this.ccp.setRefeicaoIndex(event.i)
		this.navCtrl.push("RefeicaoPage", {
			refeicao: event.r,
			refeicaoIndex: this.refeicaoIndex,
			refeicoes: this.refeicoes
		})
	}

	gerarCardapio() {
		const refeicoesBranco = []
		let touched = true
		// Checar se há refeição em dias que não foram preenchidos
		for (let i = 0; i < this.cardapio.dias.length; ++i) {
			const d = this.cardapio.dias[i]
			refeicoesBranco.push({dia: d.data, refeicao: []})

			for (let j = 0; j < d.refeicoes.length; ++j) {
				const r = d.refeicoes[j]
				if (!r.touched) {
					refeicoesBranco[i].refeicao.push(r.titulo)
					touched = false
				}
			}
		}

		// caso sim, cria um alert informando isso
		if (!touched) {
			const alert = this.alertCtrl.create({
				title: "Refeições em branco",
				message: "Você ainda tem refeições em branco. Gerar lista mesmo assim?",
				buttons: [
					{text: "Não"},
					{
						text: "Gerar a lista de compras",
						handler: data => {
							if (this.cardapio._id) {
								this.updateCardapio()
							} else {
								this.createCardapio()
							}
						}
					}
				]
			})
			alert.present()
		// se não, ele simplsmente atualiza o cardapio e cria a lista
		} else {
			if (this.cardapio._id) {
				this.updateCardapio()
			} else {
				this.createCardapio()
			}
		}
	}

	createCardapio() {
		try {
			this.showLoader()
			waterfall(
				[
					(cb) => {
						// contador de geraçõs do cardápio
						this.cardapio.gerado++

						this.cp.create(this.cardapio, {}).then(cardapio => {
							cb(undefined, cardapio)
						})
					},
					(cardapio, cb) => {
						this.lp.newLista(cardapio).then(newLista => {
							cb(undefined, newLista)
						})
					},
					(lista, cb) => {
						this.lp.create(lista, {}).then(lista => {
							cb(lista)
						})
					}
				],
				(lista) => {
					this.autosave = false
					this.navCtrl.setPages([
						{page: "HomePage"},
						{page: "ListasPage"},
						{page: "ListaPage", params: {l: lista} }
					])
					this.loading.dismiss()
				}
			)
		} catch (err) {
			console.log(err)
			this.loading.dismiss()
		}
	}

	updateCardapio() {
		try {
			this.showLoader()
			waterfall(
				[
					(cb) => {
						// contador de geraçõs do cardápio
						this.cardapio.gerado++

						this.cp.update(this.cardapio, {}).then(cardapio => {
							cb(undefined, this.cardapio)
						})
					},
					(cardapio, cb) => {
						this.lp.newLista(cardapio).then(newLista => {
							cb(undefined, newLista)
						})
					},
					(lista, cb) => {
						this.lp.create(lista, {}).then(lista => {
							cb(lista)
						})
					}
				],
				(lista) => {
					this.showToast("Cardápio salvo e lista criada")
					this.autosave = false
					this.navCtrl.setPages([
						{page: "HomePage"},
						{page: "ListasPage"},
						{page: "ListaPage", params: {l: lista} }
					])
					this.loading.dismiss()
				}
			)
		} catch (err) {
			console.log(err)
			this.loading.dismiss()
		}
	}

	cardapioDetalhesModal() {
		const modal = this.modalCtrl.create("CardapioDetalhesPage", {cardapio: this.cardapio}, {enableBackdropDismiss: false})
		modal.onDidDismiss(data => {
			if (data) {
				this.cardapio = this.ccp.newCardapio(data)
				this.cardapioIndex = this.ccp.setCardapioIndex(0)
				this.modelCardapioDia = this.ccp.setModelCardapioDia(this.cardapio.dias[this.cardapioIndex])
				this.modelCardapioDiaClone = this.up.clone(this.modelCardapioDia)

				this.refeicoes = this.rp.getRefeicoes({})
				this.refeicaoIndex = this.ccp.setRefeicaoIndex(0)
				this.modelRefeicao = this.refeicoes[this.refeicaoIndex]

				this.cardapioClone = this.up.clone(this.cardapio)
				this.orderByReceitas("normal")
			}
		})
		modal.present()
	}

	copiarDia(event) {
		if ((event.c + event.i) >= this.cardapio.dias.length) {
			this.cardapioIndex = this.ccp.setCardapioIndex(0)
		} else if ((event.c + event.i) < 0) {
			this.cardapioIndex = this.ccp.setCardapioIndex(this.cardapio.dias.length - 1)
		} else {
			this.cardapioIndex = this.ccp.setCardapioIndex(event.c + event.i)
		}
		this.modelCardapioDia = this.ccp.setModelCardapioDia(this.cardapio.dias[this.cardapioIndex])
	}

	modalCopiarDia() {
		const modal = this.modalCtrl.create(
			"RefeicaoCopiarPage",
			{
				cardapio: this.cardapio,
				refeicoes: this.refeicoes,
				cardapioIndex: this.cardapioIndex
			},
			{
				enableBackdropDismiss: false
			}
		)
		modal.onDidDismiss(data => {
			console.log(data)
		})
		modal.present()
	}

	limparCardapio() {
		const alert = this.alertCtrl.create({
			title: "Limpar",
			message: "Deseja limpar o cardápio?"
		})
		alert.addInput({
			type: "radio",
			label: "Hoje",
			value: "hoje",
		})
		alert.addInput({
			type: "radio",
			label: "Todos os dias",
			value: "todos_os_dias",
		})
		alert.addButton({
			text: "OK",
			handler: data => {
				if (data === "hoje") {
					this.cardapio.dias[this.cardapioIndex].refeicoes = this.ccp.resetRefeicao(this.cardapioIndex)
					this.modelCardapioDia = this.ccp.setModelCardapioDia(this.cardapio.dias[this.cardapioIndex])
					this.modelCardapioDiaClone = this.up.clone(this.modelCardapioDia)
				} else {
					for (let i = this.cardapio.dias.length - 1; i >= 0; i--) {
						this.cardapio.dias[i].refeicoes = this.ccp.resetRefeicao(i)
					}
					this.cardapioClone = this.up.clone(this.cardapio)
				}
			}
		});
		alert.present()
	}

	alterarFiltros() {
		this.filtro.open()
	}

	alterarRefeicao(event) {
		if ((event.r.ordem + event.i) >= this.refeicoes.length) {
			this.refeicaoIndex = this.ccp.setRefeicaoIndex(0)
		} else if ((event.r.ordem + event.i) < 0) {
			this.refeicaoIndex = this.ccp.setRefeicaoIndex( this.refeicoes.length - 1 )
		} else {
			this.refeicaoIndex = this.ccp.setRefeicaoIndex(event.r.ordem + event.i)
		}
		this.modelRefeicao = this.refeicoes[this.refeicaoIndex]
		this.orderByReceitas("normal")
	}

	orderByReceitas(orderBy) {
		this.orderBy = orderBy
		this.getReceitas({search: this.search, orderBy})
	}

	searchReceitas(ev) {
		this.search = ev.target.value
		if (this.search && this.search.trim() !== "") {
			this.getReceitas({search: this.search, orderBy: this.orderBy})
		}
	}

	// Função para pegar receitas
	getReceitas(params) {
		const data: any = {user_id: this.ap.getCurrentUser()._id, tag_refeicao: this.modelRefeicao.tag, ...params}
		const orderBy = (ar, currentField, orderType) => {
			return ar.sort(function(a, b) {
				if (orderType === "ASC") {
					if (a[currentField] < b[currentField]) return -1
					if (a[currentField] > b[currentField]) return 1
					return 0
				} else {
					if (a[currentField] < b[currentField]) return 1
					if (a[currentField] > b[currentField]) return -1
					return 0
				}
			})
		}

		try {
			this.showLoader()
			this.recP.getReceitasFiltered(data, {}).then((receitas) => {
				this.receitas = receitas

				const parentes = []
				const filhos = []
				const orderType = "DESC"
				const currentField = "score"

				// Separa quais tags são tipo "parente" e tipo "filho"
				for (let r, i = this.receitas.length - 1; i >= 0; i--) {
					r = this.receitas[i]
					if (r.tags.tipo === "parente") { parentes.push( {i, _id: r.tags._id} ) } // Nas tags parente, guarda o índice que a tag está no array de receitas, e o _id para comparar com o parent_id
					if (r.tags.tipo === "filho") { filhos.push(r) } // Nas tags filho, guarda o parent_id e ele
					r.receitas = orderBy(r.receitas, currentField, orderType)
				}

				// Construindo a hierarquia de pai e filho

				// Para cada parente
				for (let p, j = parentes.length - 1; j >= 0; j--) {
					p = parentes[j]

					// Seta receitas deste parente como um array vazio
					this.receitas[p.i].receitas = []
					// Para cada filho
					for (let f, k = filhos.length - 1; k >= 0; k--) {
						f = filhos[k]
						// Se f for filho de p, adiciona na array de receitas
						if (f.tags.parent_id === p._id) {
							this.receitas[p.i].receitas.push(f)
							filhos.splice(k, 1)
						}
					}
					this.receitas[p.i].receitas = orderBy(this.receitas[p.i].receitas, currentField, orderType)
					this.receitas[p.i].receitas.sort(function(a, b) {return a.tags.titulo > b.tags.titulo})
				}
				this.receitas = this.receitas.filter((r) => {
					if (r.tags.tipo === "filho") {
						return false
					}
					if (r.tags.tipo === "refeicao") {
						return false
					}
					return true
				})
				this.loading.dismiss()
			})
		} catch (err) {
			console.error(err)
			this.loading.dismiss()
		}
	}

	receitaModal(r) {
		const modal = this.modalCtrl.create("ReceitaPage", {
			receita: r,
		})
		modal.onDidDismiss(adicionar => {
			if (adicionar) {
				this.adicionarReceita(r)
			}
		})
		modal.present()
	}

	adicionarReceita(r) {
		this.modelCardapioDia = this.ccp.getModelCardapioDia()
		this.modelCardapioDia.refeicoes[this.refeicaoIndex].touched = true
		this.modelCardapioDia.refeicoes[this.refeicaoIndex].receitas.push( this.up.clone(r))
		this.showToast("Receita adicionada")
	}

	toggleSearch() {
		this.searchToggle = !this.searchToggle
	}

	ionViewWillUnload() {
		if (this.autosave) {
			if (this.up.isDifferent(this.cardapio, this.cardapioClone)) {
				const alert = this.alertCtrl.create({
					title: "Cardápio alterado",
					message: "Deseja salvar alterações?",
					buttons: [
						{text: "Não"},
						{
							text: "Sim",
							handler: data => {
								this.cp.create(this.cardapio, {}).then(cardapio => {
									this.showToast("Cardápio salvo")
								})
							}
						}
					]
				})
				alert.present()
			}
		}
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		})
		toast.present()
	}

	pop() {
		if (this.navCtrl.canGoBack()) {
			this.navCtrl.pop()
		} else {
			this.rootHome()
		}
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}

}