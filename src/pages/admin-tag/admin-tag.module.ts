import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AdminTagPage } from "./admin-tag";

@NgModule({
	declarations: [
		AdminTagPage,
	],
	imports: [
		IonicPageModule.forChild(AdminTagPage),
	],
})
export class AdminTagPageModule {}
