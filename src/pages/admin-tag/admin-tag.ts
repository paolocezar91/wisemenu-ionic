import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, Events } from "ionic-angular"
import { AuthProvider } from "../../providers/auth"
import { TagsProvider } from "../../providers/tags"

import _ from "lodash"

/**
 * Generated class for the AdminTagPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
	segment: "admin-tag/:_id"
})
@Component({
	selector: "page-admin-tag",
	templateUrl: "admin-tag.html",
	providers: [TagsProvider]
})
export class AdminTagPage {

	tag: any
	tagClone: any
	loading: any

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private tp: TagsProvider,
		private ap: AuthProvider,
		private loadingCtrl: LoadingController,
		private alertCtrl: AlertController,
		private toastCtrl: ToastController,
		private events: Events,
	) {
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad AdminReceita")
		this.init()
	}

	ionViewCanEnter() {
		return new Promise((resolve, reject) => {
			// Checagem de role do usuário
			this.ap.currentUserRoleCheck("admin").then((res) => {
				if (!res) {
					reject("Não autorizado")
				} else {
					resolve(true)
				}
			})
		}).catch(err => this.navCtrl.setRoot("HomePage"))
	}

	ionViewWillLeave() {
		if (this.isDifferent()) {
			const alert = this.alertCtrl.create({
				title: "Receita alterada",
				message: "Deseja salvar alterações?",
				buttons: [
					{
						text: "Não",
						handler: data => {}
					},
					{
						text: "Sim",
						handler: data => {
							this.createOrUpdateTag()
						}
					}
				]
			})
			alert.present()
		}
		this.events.unsubscribe("reloadPage")
	}

	init() {
		try {
			this.getTag()
		} catch (err) {
			console.log(err)
		}
	}

	getTag() {
		if (this.navParams.get("_id") !== "new") {
			const data = {_id: this.navParams.get("_id")}, options = {}
			this.showLoader()
			this.tp.getOne(data, options).then(tag => {
				this.tag = tag
				this.tagClone = this.clone(this.tag)
				this.loading.dismiss()
			}).catch(err => {
				this.loading.dismiss()
			})
		} else {
			this.tag = {
				titulo: "",
				tipo: ""
			}
			this.tagClone = this.clone(this.tag)
		}
	}

	createOrUpdateTag() {
		this.showLoader()
		this.tp.createOrUpdate(this.tag, {}).then(tag => {
			// if its a create, should reload on back to previous page
			if (!tag["ok"]) {
				this.events.publish("reloadPage")
				this.tag = tag
				this.tagClone = this.clone(this.tag)
			} else {
				this.tagClone = this.clone(this.tag)
			}
			this.showToast("As alterações foram salvas")
			this.loading.dismiss()
		})
	}

	rootAdminHome() {
		this.navCtrl.setRoot("AdminHomePage")
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})

		this.loading.present()
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		})
		toast.present()
	}

	clone(original) {
		return JSON.parse(JSON.stringify(original))
	}

	isDifferent() {
		return !_.isEqual(this.tag, this.tagClone)
	}

}
