import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CardapiosPage } from "./cardapios";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
	declarations: [
		CardapiosPage,
	],
	imports: [
		IonicPageModule.forChild(CardapiosPage),
		PipesModule
	],
	entryComponents: [
		CardapiosPage
	]
})
export class CardapiosModule {}
