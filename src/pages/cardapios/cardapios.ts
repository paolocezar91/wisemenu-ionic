import { Component } from "@angular/core"
import { IonicPage, NavController, ModalController, AlertController, LoadingController } from "ionic-angular"
import { CardapiosProvider } from "../../providers/cardapios"
import { CurrentCardapiosProvider } from "../../providers/current-cardapios"
import { AuthProvider } from "../../providers/auth"
import _ from "lodash"


/*
	Generated class for the Cardapios page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-cardapios",
	templateUrl: "cardapios.html",
	providers: [CardapiosProvider]
})
export class CardapiosPage {

	cardapios: any
	user: any
	guest: any = false
	_: any = _

	loading: any

	constructor(
		public navCtrl: NavController,
		public modalCtrl: ModalController,
		public alertCtrl: AlertController,
		private cp: CardapiosProvider,
		private ccp: CurrentCardapiosProvider,
		private ap: AuthProvider,
		private loadingCtrl: LoadingController
	) {
		this.showLoader()
		this.user = this.ap.getCurrentUser()
	}

	ionViewDidLoad() {
		this.init()
		console.log("Hello Cardapios Page")
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})
		this.loading.present()
	}

	init() {
		const data = {user_id: this.user._id}
		const options = {select: "titulo dias.data de ate porcoes diasCounter"}

		this.cp.getAll(data, options).then(cardapios => {
			this.cardapios = this.ccp.setCardapio(cardapios)

			this.loading.dismiss()
		})
	}

	pushCardapioPage(c) {
		this.navCtrl.push("CardapioPage", {c})
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}


	editCardapioDetalhesModal(c) {
		const modal = this.modalCtrl.create("CardapioDetalhesPage", {cardapio: c})
		modal.onDidDismiss(data => {
			if (data === undefined) {
				this.rootHome()
			}
		})
		modal.present()
	}

	removeCardapio(c) {
		const alert = this.alertCtrl.create({
				title: "Remover cardápio?",
				message: "Remover <strong>" + c.titulo + "</strong>?",
				buttons: [
					{text: "Não"},
					{
						text: "Sim",
						handler: data => {
							this.cp.delete({user_id: this.user._id, _id: c._id}).then(resolve => {
								this.cardapios = this.cardapios.filter(i => i !== c)
							})
						}
					}
				]
			})

		alert.present()
	}

	adicionarCardapio() {
		this.navCtrl.push("CardapioPage")
	}

	isGuest() {
		this.ap.currentUserRoleCheck("guest")
			.then(res => {
				this.guest = res
			})
			.catch(err => {
				this.guest = false
			})
	}

	registerPage() {
		this.navCtrl.setRoot("RegisterPage", {nextPage: "CardapiosPage"})
	}


}
