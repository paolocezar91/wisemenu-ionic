import { Component, Inject } from "@angular/core";
import { IonicPage, ViewController, NavParams, LoadingController, ToastController } from "ionic-angular";
import { ReceitasProvider } from "../../providers/receitas";
import { FavoritosProvider } from "../../providers/favoritos";
import { LikesProvider } from "../../providers/likes";
import { AuthProvider } from "../../providers/auth";
import { ENV } from "../../env/environment-variables.token";

/*
	Generated class for the Receita page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-receita",
	templateUrl: "receita.html",
	providers: [ReceitasProvider, FavoritosProvider, LikesProvider]
})
export class ReceitaPage {

	receita: any;
	favorito: any = {active: false}
	like: any = {active: false}

	loading: any

	constructor(
		public viewCtrl: ViewController,
		private navParams: NavParams,
		private rp: ReceitasProvider,
		private favsp: FavoritosProvider,
		private lp: LikesProvider,
		private ap: AuthProvider,
		private loadingCtrl: LoadingController,
		private toastCtrl: ToastController,
		@Inject(ENV) public ENV
	) {
		this.showLoader();
	}

	ionViewDidLoad() {
		console.log("Hello Receita Page");
		this.init();
	}

	init() {
		const data = {_id: this.navParams.get("receita")._id}
		const options = {}
		this.rp.getReceita(data, options).then(receita => {
			this.receita = receita;

			const fav_data = {user_id: this.ap.getCurrentUser()._id, receita_id: this.receita._id };
			this.favsp.getOne(fav_data, {}).then(favorito => {
				this.favorito = favorito;
				this.receita.favorito = true
			});

			const like_data = {user_id: this.ap.getCurrentUser()._id, receita_id: this.receita._id };
			this.lp.getOne(like_data, {}).then(like => {
				this.like = like;
			});

			this.loading.dismiss();
		});
	}

	updateFav() {
		this.showLoader();

		this.favorito.user_id = this.ap.getCurrentUser()._id;
		this.favorito.receita_id = this.receita._id;
		this.favorito.active = !this.favorito.active;

		this.favsp.update(this.favorito, {}).then(favorito => {
			const _f: any = favorito;

			if (this.favorito.active) {
				this.showToast("Receita adicionada aos favoritos");
			} else {
				this.showToast("Receita removida dos favoritos");
			}

			if (this.favorito._id === undefined) {
				this.favorito._id = _f.upserted[0]._id
			}
			this.loading.dismiss();
		});
	}

	updateLike() {
		this.showLoader();

		this.like.likeData.user_id = this.ap.getCurrentUser()._id;
		this.like.likeData.receita_id = this.receita._id;
		this.like.likeData.active = !this.like.likeData.active;



		this.lp.update(this.like.likeData, {}).then(like => {
			const _f: any = like;

			if (this.like.likeData.active) {
				this.like.count++;
				this.showToast("Receita curtida");
			} else {
				this.showToast("Receita descurtida");
				this.like.count--;
			}

			if (this.like.likeData._id === undefined) {
				this.like.likeData._id = _f.upserted[0]._id
			}

			this.loading.dismiss();
		});
	}

	dismiss(data?) {
		this.viewCtrl.dismiss(data);
	}

	avaliar(v) {
		const data = {_id: this.receita._id, user_id: this.ap.getCurrentUser()._id, valor: v}
		const options = {};
		this.rp.avaliarReceita(data, options).then(result => {
			this.showToast("Receita avaliada")
		});
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		});
		toast.present();
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		});

		this.loading.present();
	}

	sharePage() {
		alert("Compartilhar");
	}

}
