import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReceitaPage } from "./receita";
import { Gradacao } from "../../components/gradacao/gradacao";
import { Ionic2RatingModule } from "ionic2-rating";




@NgModule({
	declarations: [
		ReceitaPage,
		Gradacao,
	],
	imports: [
		IonicPageModule.forChild(ReceitaPage),
		Ionic2RatingModule
	],
	entryComponents: [
		ReceitaPage
	]
})
export class ReceitaModule {}
