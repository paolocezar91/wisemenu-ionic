import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AdminReceitas_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-receitas-2',
  templateUrl: 'admin-receitas-2.html',
})
export class AdminReceitas_2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminReceitas_2Page');
  }

  rootAdminHome() {
		this.navCtrl.setRoot("AdminHomePage")
  }

  pushPage(p, param) {
  	this.navCtrl.push(p, {title: param});
	}

}
