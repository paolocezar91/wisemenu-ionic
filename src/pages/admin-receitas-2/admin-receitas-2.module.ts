import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminReceitas_2Page } from './admin-receitas-2';

@NgModule({
  declarations: [
    AdminReceitas_2Page,
  ],
  imports: [
    IonicPageModule.forChild(AdminReceitas_2Page),
  ],
})
export class AdminReceitas_2PageModule {}
