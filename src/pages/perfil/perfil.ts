import { Component } from "@angular/core";
import { IonicPage, NavController, LoadingController } from "ionic-angular";
import { AuthProvider } from "../../providers/auth";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { matchingPasswords } from "../../validators/password";

/*
	Generated class for the Perfil page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-perfil",
	templateUrl: "perfil.html",
})
export class PerfilPage {

	user: any
	submitAttempt: boolean
	resetPasswordForm: FormGroup
	resetPasswordMessage: any
	loading: any

	constructor(
		public navCtrl: NavController,
		public loadingCtrl: LoadingController,
		private ap: AuthProvider,
		public formBuilder: FormBuilder
	) {
		this.showLoader();
	}

	ionViewDidLoad() {
		console.log("Hello Perfil Page");
		this.init();
	}

	init() {
		try {
			this.submitAttempt = false;
			this.resetPasswordForm = this.formBuilder.group({
				password: ["", Validators.required],
				newPassword: ["", Validators.required],
				repeatNewPassword: ["", Validators.required],
			}, {
				validator: matchingPasswords("newPassword", "repeatNewPassword")
			});

			this.user = this.ap.getCurrentUser();
			this.loading.dismiss();
		} catch (err) {
			console.log(err);
			this.loading.dismiss();
		}
	}

	resetPassword(value) {
		this.submitAttempt = true;

		this.showLoader();

		this.ap.resetPassword(value).then((result) => {
			this.loading.dismiss();
			this.resetPasswordMessage = result;
		});
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}


	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		});
		this.loading.present();
	}

}
