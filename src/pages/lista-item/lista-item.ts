import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, ViewController } from "ionic-angular";
import { ProdutosProvider } from "../../providers/produtos";

/**
 * Generated class for the ListaNovoItem page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-lista-item",
	templateUrl: "lista-item.html",
	providers: [ProdutosProvider]
})
export class ListaItemPage {

	item: any
	itemForm: any

	searching: Boolean = false
	searchText: any
	searchDisabled: any = false
	filterResults: any = []

	loading: any
	errorMessage: any

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		public pp: ProdutosProvider,
		private toastCtrl: ToastController,
	) {
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad ListaItemPage");
		this.init();
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		});
		this.loading.present();
	}

	init() {
		this.item = this.navParams.get("item");

		this.searchDisabled = this.item.produto_id._id ? true : false;

		this.itemForm = {
			_id: this.item.produto_id._id || "",
			titulo: this.item.produto_id.titulo || "",
			valor: this.item.valor || "",
			quantidade: this.item.quantidade || "",
			unidade: this.item.unidade || "",
			check: this.item.check || false
		}

		console.log(this.itemForm)

		this.searchText = this.item.produto_id.titulo || ""
	}

	add(itemForm) {
		this.showLoader();

		this.errorMessage = "";

		// Se há erros de validação do formulário
		if (!itemForm) {
			if (itemForm.titulo) {
				this.errorMessage += "Você deve digitar o nome do produto. <br>"
			}
			if (itemForm.quantidade) {
				this.errorMessage += "Você deve digitar uma quantidade para o produto. <br>"
			}

			console.log(itemForm.controls)

			this.loading.dismiss();
			this.showError(this.errorMessage);

		} else {
			const item = {
				produto_id: {
					_id: itemForm._id || false,
					titulo: itemForm.titulo
				},
				valor: itemForm.valor,
				quantidade: itemForm.quantidade,
				unidade: itemForm.unidade,
				check: itemForm.check || false,
			};
			// Se o produto não tem _id, não existe no banco e deve ser criado.
			if (!item.produto_id._id) {
				const produto = {
					titulo: item.produto_id.titulo,
					sinonimos: [],
					unidades_domesticas: [],
					unidades_comerciais: [],
					marcas_id: []
				};

				this.pp.create(produto).then((produto: any) => {
					item.produto_id._id = produto._id;
					this.loading.dismiss();
					this.dismiss(item);
				});
			} else {
				this.loading.dismiss();
				this.dismiss(item);
			}
		}
	}

	search(e?) {
		this.searching = true;
		this.searchText = this.trim(e.srcElement.value);

		if (this.searchText) {
			if (this.searchText !== "") {
				setTimeout(() => {
					this.pp.getResults(this.searchText).then(filterResults => {
						this.filterResults = filterResults;
						this.searching = false;
					});

					this.itemForm.controls["titulo"].setValue(this.searchText);
				}, 500)
			}
		} else {
			this.clear();
			this.searching = false;
		}
	}

	clear() {
		this.searchText = "";
		this.itemForm.controls["titulo"].setValue("");
		this.itemForm.controls["_id"].setValue("");
		this.filterResults = [];
	}

	blur() {
		setTimeout(() => {
			this.filterResults = [];
		}, 300);
	}

	selectItem(e) {
		this.searchText = e.titulo;
		this.itemForm.controls["titulo"].setValue(e.titulo);
		this.itemForm.controls["_id"].setValue(e._id);
		this.filterResults = [];
	}

	trim(value) {
		if (value) {
			return value.replace (/^\s+/gi, "") // removes leading and trailing spaces
				.replace (/[ ]{2,}/gi, " ")       // replaces multiple spaces with one space
				.replace (/\n +/, "\n");         // Removes spaces after newlines
		} else {
			return undefined
		}
	}​

	dismiss(data?) {
		if (data !== undefined) {
			this.viewCtrl.dismiss(data);
		} else {
			this.viewCtrl.dismiss();
		}
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		})
		toast.present();
	}

	showError(errorMessage) {
		const alert = this.alertCtrl.create({
				title: "Erro",
				message: errorMessage,
				buttons: [
					{text: "Ok"}
				]
		});
		alert.present();
	}
}
