import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ListaItemPage } from "./lista-item";
import { PipesModule } from "../../pipes/pipes.module";


@NgModule({
	declarations: [
		ListaItemPage,
	],
	imports: [
		IonicPageModule.forChild(ListaItemPage),
		PipesModule
	],
	exports: [
		ListaItemPage
	]
})
export class ListaNovoItemModule {}
