import { Component } from "@angular/core"
import { IonicPage, NavController, ToastController, LoadingController, AlertController } from "ionic-angular"
import { TagsProvider } from "../../providers/tags"
import { FiltrosProvider } from "../../providers/filtros"
import { AuthProvider } from "../../providers/auth"
import { UtilsProvider } from "../../providers/utils"
import waterfall from "async-es/waterfall"

/*
	Generated class for the Filtros page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-filtros",
	templateUrl: "filtros.html",
	providers: [TagsProvider, FiltrosProvider]
})
export class FiltrosPage {

	tipo_tags: any
	filtro: any
	filtroClone: any
	segment: any = "restricoes"
	loading: any

	constructor(
		public navCtrl: NavController,
		private tp: TagsProvider,
		private fp: FiltrosProvider,
		private ap: AuthProvider,
		private up: UtilsProvider,
		private alertCtrl: AlertController,
		private toastCtrl: ToastController,
		private loadingCtrl: LoadingController
	) {

	}

	ionViewDidLoad() {
		console.log("Hello Filtros Page")
		this.init()
	}

	ionViewWillLeave() {
		if (this.up.isDifferent(this.filtro, this.filtroClone)) {
			const alert = this.alertCtrl.create({
				title: "Filtros alterados",
				message: "Deseja salvar alterações?",
				buttons: [
					{
						text: "Não",
					},
					{
						text: "Sim",
						handler: data => {
							this.updateFiltro()
						}
					}
				]
			})

			alert.present()
		}
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})
		this.loading.present()
	}

	init() {
		this.showLoader()
		try {
			// Testa se usuário é visitante, e se for, não permite a entrada na página, redirecionando para o registro
			this.ap.currentUserRoleCheck("guest")
				.then(res => {
					if (!res) {
						this.loading.dismiss()
						this.getFiltro()
					} else {
						const registerAlert = this.alertCtrl.create({
							enableBackdropDismiss: false,
							title: "Registrar",
							message: "Para alterar os filtros você precisa se registrar.",
							buttons: [
								{
									text: "Voltar",
									handler: data => {
										this.loading.dismiss()
										if (this.navCtrl.canGoBack()) {
											this.navCtrl.pop()
										} else {
											this.navCtrl.setRoot("HomePage")
										}
									}
								},
								{
									text: "Registrar",
									handler: data => {
										this.loading.dismiss()
										this.navCtrl.setRoot("RegisterPage", {nextPage: "FiltrosPage"})
									}
								}
							]
						})
						registerAlert.present()
					}
				}).catch(err => {
					this.loading.dismiss()
					this.navCtrl.setRoot("HomePage")
				})
		} catch (err) {
			this.loading.dismiss()
			this.navCtrl.setRoot("HomePage")
		}
	}

	getFiltro() {
		this.showLoader()
		waterfall(
			[
				// Callback para pegar os filtros do cliente
				(cb) => {
					const data = {
						user_id: this.ap.getCurrentUser()._id
					}

					const options = {
						populate: {path: "restricoes.tags"},
						sort: {"restricoes.tags": 1}
					}

					this.fp.getOne(data, options).then(filtro => {
						this.filtro = filtro

						if (this.filtro.restricoes.collapse === undefined) {
							this.filtro.restricoes["collapse"] = Array(4).fill(false)
						}
						this.filtroClone = this.up.clone(this.filtro)
						cb(undefined)
					})
				},
				// Callback para pegar as tags
				(cb) => {
					const data = {}
					const options = {aggregate: true, match: ["filho", "normal", "nacionalidade"]}

					this.tp.getAll(data, options).then(tipo_tags => {
						this.tipo_tags = tipo_tags
						this.tipo_tags = [
							{
								tipo: this.tipo_tags[0].tipo,
								tags: this.tipo_tags[0].tags
									.concat(this.tipo_tags[1].tags)
									.sort((a, b) => {
											if (a.titulo < b.titulo) {return -1}
											if (a.titulo > b.titulo) {return 1}
											return 0
										}
									)
							}, {
								tipo: this.tipo_tags[2].tipo,
								tags: this.tipo_tags[2].tags
									.sort((a, b) => {
											if (a.titulo < b.titulo) {return -1}
											if (a.titulo > b.titulo) {return 1}
											return 0
										}
									)
							}
						]
						cb(undefined)
					})
				}
			],
			() => {
				this.loading.dismiss()
			}
		)
	}

	updateFiltro() {
		this.showLoader()

		return new Promise(resolve => {
			this.fp.update(this.filtro, {}).then(filtro => {
				this.filtroClone = this.up.clone(this.filtro)

				this.loading.dismiss()
				const _f: any = filtro
				if (_f.upserted) {
					this.filtro._id = _f.upserted[0]._id
				}
				this.showToast("As alterações foram salvas")
				resolve(_f)
			})
		})
	}

	pop() {
		if (this.up.isDifferent(this.filtro, this.filtroClone)) {
			const alert = this.alertCtrl.create({
				title: "Filtros alterados",
				message: "Deseja salvar alterações?",
				buttons: [
					{
						text: "Não",
						handler: data => {
							this.rootHome()
						}
					},
					{
						text: "Sim",
						handler: data => {
							this.updateFiltro().then(_l => {
								this.rootHome()
							})
						}
					}
				]
			})

			alert.present()
		} else {
			this.rootHome()
		}
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 3000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		})
		toast.present()
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}
}