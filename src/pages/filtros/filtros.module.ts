import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FiltrosPage } from "./filtros";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
	declarations: [
		FiltrosPage,
	],
	imports: [
		IonicPageModule.forChild(FiltrosPage),
		ComponentsModule,
		PipesModule
	],
	entryComponents: [
		FiltrosPage
	]
})
export class FiltrosModule {}
