import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth";

/**
 * Generated class for the AdminHome page.
 *
 * See http://ionicframework.com/docs/components/#navig for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-admin-home",
	templateUrl: "admin-home.html",
})
export class AdminHomePage {

	constructor(
		public navCtrl: NavController,
		public ap: AuthProvider,
	) {

	}

	ionViewCanEnter() {
		return new Promise((resolve, reject) => {
			// Checagem de role do usuário
			this.ap.currentUserRoleCheck("admin").then((res) => {
				if (!res) {
					reject("Não autorizado")
				} else {
					resolve(true)
				}
			})
		}).catch(err => this.navCtrl.setRoot("HomePage"))
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad AdminHome");
	}

	returnMenu() {
		return [
			{
				id: "receitas",
				title: "Receitas",
				callback: () => this.pushPage("AdminReceitasPage")
			},
			{
				id: "tags",
				title: "Tags",
				callback: () => {this.pushPage("AdminTagsPage")}
			},
			{
				id: "receitas2",
				title: "Receitas2",
				callback: () => {this.pushPage("AdminReceitas_2Page")}
			}
		];
	}

	pushPage(p) {
		this.navCtrl.push(p);
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}

}
