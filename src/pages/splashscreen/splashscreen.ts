import { Component } from "@angular/core";
import { IonicPage, NavController, LoadingController, AlertController } from "ionic-angular";
import { AuthProvider } from "../../providers/auth";
import { Device } from "@ionic-native/device";
import { Facebook } from "@ionic-native/facebook";


/*
	Generated class for the Login page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/

@IonicPage()
@Component({
	selector: "page-splashscreen",
	templateUrl: "splashscreen.html",
	providers: [Device, Facebook]
})

export class SplashscreenPage {

	user: any
	errorMessage: any = "";
	loading: any;

	constructor(
		public navCtrl: NavController,
		private ap: AuthProvider,
		private loadingCtrl: LoadingController,
		private alertCtrl: AlertController,
		private device: Device,
		private fb: Facebook
	) {
	}

	ionViewDidLoad() {
		console.log("Hello LoginPage Page");
	}

	facebookLogin() {
		// the permissions your facebook app needs from the user
		const permissions = ["public_profile"];

		this.showLoader();

		this.fb.login(permissions).then((response) => {
			const userId = response.authResponse.userID;
			const params = new Array();

			// Getting name and gender properties
			this.fb.api("/me?fields=name,gender", params).then((user) => {
				const credentials = {
					name: user.name,
					uuid: this.device.uuid,
					facebook_id: userId
				};
				this.ap.createFBAccount(credentials).then((result) => {
					this.loading.dismiss();
					this.navCtrl.setRoot("HomePage");
				}, (err) => {
					console.log("err", err)
					this.loading.dismiss();
					if (err.status === 0) {
						this.errorMessage = "Houve problemas de comunicação com o servidor. Tente mais tarde."
					} else {
						this.errorMessage = JSON.parse(err._body).error;
					}
					this.showError(this.errorMessage);
				});
			})
		}, function(error){
			console.log(error);
		});
	}

	guestRegister() {
		this.showLoader();

		const credentials = {
			name: "Visitante",
			uuid: this.device.uuid,
			guest: true
		};

		this.ap.createGuestAccount(credentials)
			.then((result) => {
				this.loading.dismiss();
				this.navCtrl.setRoot("HomePage");
			})
			.catch((err) => {
				console.log("err", err)
				this.loading.dismiss();
				if (err.status === 0) {
					this.errorMessage = "Houve problemas de comunicação com o servidor. Tente mais tarde."
				} else {
					this.errorMessage = JSON.parse(err._body).error;
				}
				this.showError(this.errorMessage);
			});
	}

	pushPage(p) {
		this.navCtrl.push(p);
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		});
		this.loading.present();
	}

	showError(errorMessage) {
		const alert = this.alertCtrl.create({
				title: "Erro",
				message: errorMessage,
				buttons: [
					{text: "Ok"}
				]
			});

		alert.present();
	}

}