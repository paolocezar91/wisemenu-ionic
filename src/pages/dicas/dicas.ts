import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

/**
 * Generated class for the Dicas page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-dicas",
	templateUrl: "dicas.html",
})
export class DicasPage {

	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad Dicas");
	}

	rootHome() {
		this.navCtrl.setRoot("HomePage")
	}

}
