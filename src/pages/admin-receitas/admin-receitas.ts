import { Component, Inject, ViewChild, ElementRef } from "@angular/core"
import { IonicPage, NavController, LoadingController, Events } from "ionic-angular"
import { AuthProvider } from "../../providers/auth"
import { ReceitasProvider } from "../../providers/receitas"
import { ENV } from "../../env/environment-variables.token"

/**
 * Generated class for the AdminReceitas page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-admin-receitas",
	templateUrl: "admin-receitas.html",
	providers: [ReceitasProvider]
})
export class AdminReceitasPage {

	@ViewChild("fileInput") fileInput: ElementRef

	filesToUpload: any
	items: any
	// este objeto direciona a criação da table-admin com os parametros corretos
	options: any = {
		cols: [
			{
				label: "Título",
				attr: "titulo"
			}
		],
		page: 1,
		limit: 10,
		buttons: [
			{
				label: "Novo",
				color: "primary",
				icon: "fa-plus",
				callback: () => { this.pushAdminReceitaPage({_id: "new"}) }
			},
			{
				label: "Importar .xlsx",
				color: "verde",
				icon: "fa-file-excel-o",
				callback: () => { this.importExcel() }
			}
		]
	}

	loading: any

	constructor(
		public navCtrl: NavController,
		public ap: AuthProvider,
		public rp: ReceitasProvider,
		private loadingCtrl: LoadingController,
		private events: Events,
		@Inject(ENV) public ENV
	) {
	}

	ionViewCanEnter() {
		return new Promise((resolve, reject) => {
			// Checagem de role do usuário
			this.ap.currentUserRoleCheck("admin").then((res) => {
				if (!res) {
					reject("Não autorizado")
				} else {
					resolve(true)
				}
			})
		}).catch(err => this.navCtrl.setRoot("HomePage"))
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad AdminReceitas")
		this.getReceitas(this.options.page, this.options.limit)
	}

	getReceitas(page, limit) {
		try {
			const data = {}, options = {"page": page, "limit": limit, select: "titulo"}

			this.showLoader()
			this.rp.getReceitas(data, options).then(receitas => {
				this.items = receitas
				this.loading.dismiss()
			})
		} catch (err) {
			console.log(err)
			this.loading.dismiss()
		}

	}

	setOptions(options) {
		this.options = options
		this.getReceitas(this.options.page, this.options.limit)
	}

	showLoader() {
		this.loading = this.loadingCtrl.create({
			content: "Carregando..."
		})

		this.loading.present()
	}

	importExcel() {
		this.fileInput.nativeElement.click()
	}

	uploadXls(fileInput: any) {
		this.filesToUpload = <Array<File>> fileInput.target.files

		const params = {
			attr: fileInput.target.name,
		}

		this.showLoader()

		this.rp.uploadXls(params, this.filesToUpload).then(result => {
			this.loading.dismiss()
			this.getReceitas(this.options.page, this.options.limit)
		}, (err) => {
			this.loading.dismiss()
		})
	}

	removeItem(event) {
		const data = {_id: event._id}, options = {"page": this.options.page, "limit": this.options.limit, select: "titulo"}

		this.showLoader()
		// Recarrega receitas
		this.rp.removeReceita(data, options).then(receitas => {
			this.items = receitas
			this.loading.dismiss()
		})
	}

	pushAdminReceitaPage(item) {
		this.navCtrl.push("AdminReceita", {_id: item._id})
		this.events.subscribe("reloadPage", () => {
			this.getReceitas(this.options.page, this.options.limit)
		})
	}

	rootAdminHome() {
		this.navCtrl.setRoot("AdminHomePage")
	}

}
