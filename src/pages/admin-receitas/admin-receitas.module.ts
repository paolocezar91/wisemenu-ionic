import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AdminReceitasPage } from "./admin-receitas";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
	declarations: [
		AdminReceitasPage,
	],
	imports: [
		IonicPageModule.forChild(AdminReceitasPage),
		ComponentsModule
	],
	exports: [
		AdminReceitasPage
	]
})
export class AdminReceitasModule {}
