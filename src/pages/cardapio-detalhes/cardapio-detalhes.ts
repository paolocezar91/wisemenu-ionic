import { Component, ViewChild } from "@angular/core"
import { App, ViewController, NavParams, IonicPage, Content, AlertController } from "ionic-angular"
import { LocalePipe, AddPipe } from "angular2-moment";
import * as moment from "moment"

/*
	Generated class for the CardapioDetalhes page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-cardapio-detalhes",
	templateUrl: "cardapio-detalhes.html"
})
export class CardapioDetalhesPage {

	@ViewChild(Content) content: Content

	cardapio: any = {}
	cardapioClone: any = {}
	de: any
	ate: any
	diasCounter: any

	constructor(
		public viewCtrl: ViewController,
		public navParams: NavParams,
		public localePipe: LocalePipe,
		public addPipe: AddPipe,
		public alertCtrl: AlertController,
		public appCtrl: App
	) {}

	ionViewDidLoad() {
		console.log("Hello CardapioDetalhes Page")
		this.init()
	}

	init() {
		if (this.navParams.get("cardapio")) {
			this.cardapio = this.navParams.get("cardapio")
			this.cardapioClone = this.clone(this.cardapio)
			this.de = this.cardapio.de.toISOString()
			this.ate = this.cardapio.ate.toISOString()
			this.diasCounter = this.momentDate(this.ate).diff(this.de, "days") + 1
		} else {
			this.cardapio = {
				dias: [],
				de: this.momentDate(new Date()),
				ate: this.momentDate(new Date()),
				porcoes: 1,
				diasCounter: 7
			}
			this.de = this.cardapio.de.toISOString()
			this.cardapio.ate = this.addPipe.transform(this.cardapio.de, this.cardapio.diasCounter - 1, "days")
			this.ate = this.cardapio.ate.toISOString()
			this.cardapio.titulo = "Cardápio " + this.momentDate(this.cardapio.de).format("DD[/]MM") + " à " + this.momentDate(this.cardapio.ate).format("DD[/]MM")
		}
	}

	momentDate(value) {
		value = this.localePipe.transform(value, "pt-BR")
		return value
	}

	changeDate($event) {
		if (this.diasCounter) {
			if ( this.diasCounter > (this.momentDate(this.ate).diff(this.de, "days") + 1) ) {
				const alert = this.alertCtrl.create({
					title: "Atenção",
					message: "Você está alterando para uma data menor. Isso vai remover dias do seu cardápio.",
					buttons: [
						{
							text: "Cancelar",
							role: "cancel",
							handler: data => {
								this.de = this.cardapio.de.toISOString()
								this.ate = this.cardapio.ate.toISOString()
							}
						},
						{
							text: "Ok",
							role: "confirm",
							handler: data => {
								this.cardapio.diasCounter = this.momentDate(this.ate).diff(this.de, "days") + 1
								this.cardapio.titulo = "Cardápio " + this.momentDate(this.de).format("DD[/]MM") + " à " + this.momentDate(this.ate).format("DD[/]MM")
								this.cardapio.de = this.momentDate(this.de)
								this.cardapio.ate = this.momentDate(this.ate)
							}
						}
					]
				})
				alert.present()
			} else {
				this.cardapio.diasCounter = this.momentDate(this.ate).diff(this.de, "days") + 1
				this.cardapio.titulo = "Cardápio " + this.momentDate(this.de).format("DD[/]MM") + " à " + this.momentDate(this.ate).format("DD[/]MM")
				this.cardapio.de = this.momentDate(this.de)
				this.cardapio.ate = this.momentDate(this.ate)
			}
		} else {
			this.cardapio.diasCounter = this.momentDate(this.ate).diff(this.de, "days") + 1
			this.cardapio.titulo = "Cardápio " + this.momentDate(this.de).format("DD[/]MM") + " à " + this.momentDate(this.ate).format("DD[/]MM")
			this.cardapio.de = this.momentDate(this.de)
			this.cardapio.ate = this.momentDate(this.ate)
		}
	}

	diasCounterChange($event) {
		const value = $event.srcElement.value
		if (value > 0) {
			this.cardapio.diasCounter = $event.srcElement.value
			this.cardapio.ate = this.addPipe.transform(this.cardapio.de, this.cardapio.diasCounter - 1, "days")
			this.ate = this.cardapio.ate.toISOString()
		}
	}

	resetDate() {
		this.cardapio.de = undefined
		this.cardapio.ate = undefined
		this.de = undefined
		this.ate = undefined
	}

	scrollToBottom() {
		setTimeout(() => {
			this.content.scrollToBottom()
		}, 1000)
	}

	dismiss(data?) {

		if (data !== undefined) {
			if (this.cardapio.diasCounter !== 0) {
				this.viewCtrl.dismiss(this.cardapio)
			}
		} else {
			if (!this.cardapio.dias.length) {
				this.viewCtrl.dismiss()
				if (this.appCtrl.getRootNav().canGoBack()) {
					this.appCtrl.getRootNav().pop()
				} else {
					this.appCtrl.getRootNav().setRoot("HomePage")
				}
			} else {
				this.viewCtrl.dismiss(this.cardapioClone)
			}
		}
	}

	clone(original) {
		return JSON.parse(JSON.stringify(original))
	}

	isValid() {
		if (this.cardapio.diasCounter > 0) {
			if (this.cardapio.porcoes > 0) {
				return undefined
			}
		}
		return true
	}
}
