import { NgModule } from "@angular/core"
import { IonicPageModule } from "ionic-angular"
import { CardapioDetalhesPage } from "./cardapio-detalhes"
import { MomentModule, LocalePipe, AddPipe } from "angular2-moment"


@NgModule({
	declarations: [
		CardapioDetalhesPage,
	],
	imports: [
		IonicPageModule.forChild(CardapioDetalhesPage),
		MomentModule,
	],
	providers: [LocalePipe, AddPipe],
	entryComponents: [
		CardapioDetalhesPage
	]
})
export class CardapioDetalhesModule {}
