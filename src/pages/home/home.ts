import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { AuthProvider } from "../../providers/auth";


/*
	Generated class for the Index page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-home",
	templateUrl: "home.html"
})
export class HomePage {

	menu: any

	constructor(
		public navCtrl: NavController,
		public ap: AuthProvider,
	) {
	}

	ionViewDidLoad() {
		console.log("Hello Home Page");
		this.init();
	}

	init() {
		this.menu = [{
			id: "montar-cardapio",
			icon: "fa-file-o",
			title: "Montar cardápio",
			callback: () => this.pushPage("CardapioPage")
		}, {
			id: "lista-de-compras",
			icon: "fa-align-left",
			title: "Minhas listas",
			callback: () => this.pushPage("ListasPage")
		}]
	}

	pushPage(p) {
		this.navCtrl.push(p);
	}

	logout() {
		this.ap.logout().then((result) => {
			if (result) {
				this.navCtrl.setRoot("LoginPage");
			}
		});
	}


	pushAdmin() {
		this.navCtrl.setRoot("AdminHomePage")
	}

	roleCheck(role) {
		this.ap.currentUserRoleCheck(role)
			.then(check => {
				return check
			})
	}
}
