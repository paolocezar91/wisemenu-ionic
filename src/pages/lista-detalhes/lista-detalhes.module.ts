import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ListaDetalhesPage } from "./lista-detalhes";

@NgModule({
	declarations: [
		ListaDetalhesPage,
	],
	imports: [
		IonicPageModule.forChild(ListaDetalhesPage),
	],
	exports: [
		ListaDetalhesPage
	]
})
export class ListaDetalhesModule {}
