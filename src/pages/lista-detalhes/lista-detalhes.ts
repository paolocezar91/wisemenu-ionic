import { Component } from "@angular/core";
import { IonicPage, NavController, ViewController, NavParams } from "ionic-angular";

/**
 * Generated class for the ListaDetalhes page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
	selector: "page-lista-detalhes",
	templateUrl: "lista-detalhes.html",
})
export class ListaDetalhesPage {

	lista: any

	constructor(
		public navCtrl: NavController,
		public viewCtrl: ViewController,
		public navParams: NavParams
	) {
		if (this.navParams.get("lista")) {
			this.lista = this.navParams.get("lista");
		}
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad ListaDetalhes");
	}

	dismiss(data?) {
		this.viewCtrl.dismiss();
	}

	pushCardapioPage(c) {
		this.navCtrl.setPages([
			{page: "HomePage"},
			{page: "CardapiosPage"},
			{page: "CardapioPage", params: {c}}
		]);
	}

}
