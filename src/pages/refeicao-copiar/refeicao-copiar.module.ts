import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { RefeicaoCopiarPage } from "./refeicao-copiar";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
	declarations: [
		RefeicaoCopiarPage,
	],
	imports: [
		IonicPageModule.forChild(RefeicaoCopiarPage),
		ComponentsModule,
	],
	entryComponents: [
		RefeicaoCopiarPage
	]
})
export class RefeicaoCopiarModule {}
