import { Component } from "@angular/core";
import { IonicPage, ViewController, NavController, NavParams } from "ionic-angular";
import { CurrentCardapiosProvider } from "../../providers/current-cardapios";

/*
	Generated class for the CopiarDia page.

	See http://ionicframework.com/docs/v2/components/#navigation for more info on
	Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: "page-refeicao-copiar",
	templateUrl: "refeicao-copiar.html"
})
export class RefeicaoCopiarPage {

	cardapioIndexDe: any
	cardapioIndexPara: any
	modelCardapioDiaDe: any
	modelCardapioDiaPara: any
	cardapio: any
	refeicoes: Array<any>
	refeicoesEscolhidas: Array<any>

	constructor(
		public viewCtrl: ViewController,
		public navCtrl: NavController,
		public navParams: NavParams,
		private ccp: CurrentCardapiosProvider
	) {
	}

	ionViewDidLoad() {
		console.log("Hello CopiarRefeicao Page");
		this.init();
	}

	init() {
		this.cardapio = this.navParams.get("cardapio")
		this.refeicoes = this.clone( this.navParams.get("refeicoes") );

		this.cardapioIndexPara = this.clone( this.navParams.get("cardapioIndex") )
		this.cardapioIndexDe = this.clone(this.cardapioIndexPara) === 0 ? 1 : 0;

		this.modelCardapioDiaPara = this.clone(this.cardapio.dias[this.cardapioIndexPara]);
		this.modelCardapioDiaDe = this.clone(this.cardapio.dias[this.cardapioIndexDe]);

		this.refeicoesEscolhidas = [];
	}

	dismiss(data?) {
		if (data) {
			// clona cada refeicao dos dias referenciados por cardapioIndexClone e cardap
			for (let receitasDiaClonado, refeicaoClonada, i = 0; i < this.refeicoesEscolhidas.length; ++i) {
				refeicaoClonada = this.refeicoesEscolhidas[i]
				receitasDiaClonado = this.cardapio.dias[this.cardapioIndexDe].refeicoes[(refeicaoClonada.ordem)].receitas;
				this.cardapio.dias[this.cardapioIndexPara].refeicoes[(refeicaoClonada.ordem)].receitas = this.clone(receitasDiaClonado);
			}
		}

		this.viewCtrl.dismiss();
	}

	// Adiciona a refeição "r" à lista de refeicoesEscolhidas
	copiarRefeicao(r, i) {
		this.refeicoesEscolhidas.push(r);
		this.refeicoesEscolhidas.sort((a, b) => {
			return (a.id > b.id ? 1 : 0);
		});

		this.refeicoes.splice(i, 1);
	}

	copiarDiaPara(event) {
		if ((event.c + event.i) >= this.cardapio.dias.length) {
			this.cardapioIndexPara = 0
		} else if ((event.c + event.i) < 0) {
			this.cardapioIndexPara = this.cardapio.dias.length - 1
		} else {
			this.cardapioIndexPara = event.c + event.i
		}
		this.modelCardapioDiaPara = this.clone(this.cardapio.dias[this.cardapioIndexPara])
	}

	copiarDiaDe(event) {
		if ((event.c + event.i) >= this.cardapio.dias.length) {
			this.cardapioIndexDe = 0
		} else if ((event.c + event.i) < 0) {
			this.cardapioIndexDe = this.cardapio.dias.length - 1
		} else {
			this.cardapioIndexDe = event.c + event.i
		}
		this.modelCardapioDiaDe = this.clone(this.cardapio.dias[this.cardapioIndexDe])
	}

	clone(original) {
		return JSON.parse(JSON.stringify(original));
	}

}
