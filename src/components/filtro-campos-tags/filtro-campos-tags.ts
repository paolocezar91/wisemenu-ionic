import { Component, Input, Output, EventEmitter } from "@angular/core"

/**
 * Generated class for the FiltroCamposTags component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
	selector: "filtro-campos-tags",
	templateUrl: "filtro-campos-tags.html"
})
export class FiltroCamposTags {

	@Input() tagsEscolhidas: any
	@Input() tags: any
	@Input() tipo: any
	@Output() remover: any = new EventEmitter<any>()

	constructor() {
		console.log("Hello FiltroCamposTags Component")
	}

	removerEmit(t) {
		this.remover.emit({t})
	}

}
