import { Component, Input, Output, EventEmitter } from "@angular/core"
import { CurrentCardapiosProvider } from "../../providers/current-cardapios"

/*
	Generated class for the DiaSeletor component.

	See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
	for more info on Angular 2 Components.
*/
@Component({
	selector: "dia-seletor",
	templateUrl: "dia-seletor.html"
})
export class DiaSeletor {

	@Input() modelCardapioDia: any
	@Input() cardapioIndex: any
	@Input() cardapio: any

	@Output() copiarDia = new EventEmitter<any>()
	seletor: Boolean = false


	hoje = new Date()

	constructor(
		private ccp: CurrentCardapiosProvider
	) {
		console.log("Hello DiaSeletor Component")
	}

	copiarDiaEmit(c, i) {
		this.seletor = false
		this.copiarDia.emit({c, i})
	}

	toggleSeletor() {
		this.seletor = !this.seletor
	}

}
