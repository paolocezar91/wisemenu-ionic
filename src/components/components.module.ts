import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";

import { DiaSeletor } from "./dia-seletor/dia-seletor.ts";
import { RefeicoesLista } from "./refeicoes-lista/refeicoes-lista";
import { RefeicoesEscolhidas } from "./refeicoes-escolhidas/refeicoes-escolhidas";
import { RefeicaoSeletor } from "./refeicao-seletor/refeicao-seletor";
import { ReceitasEscolhidas } from "./receitas-escolhidas/receitas-escolhidas";
import { TagsEscolhidas } from "./tags-escolhidas/tags-escolhidas";
import { FiltroCampos } from "./filtro-campos/filtro-campos";
import { FiltroCamposTags } from "./filtro-campos-tags/filtro-campos-tags";
import { AdminTable } from "./admin-table/admin-table";

import { PipesModule } from "../pipes/pipes.module";


@NgModule({
	declarations: [
		DiaSeletor,
		RefeicoesLista,
		RefeicoesEscolhidas,
		ReceitasEscolhidas,
		RefeicaoSeletor,
		TagsEscolhidas,
		FiltroCamposTags,
		FiltroCampos,
		AdminTable,
	],
	imports: [
		IonicPageModule.forChild(DiaSeletor),
		IonicPageModule.forChild(RefeicoesLista),
		IonicPageModule.forChild(RefeicoesEscolhidas),
		IonicPageModule.forChild(ReceitasEscolhidas),
		IonicPageModule.forChild(RefeicaoSeletor),
		IonicPageModule.forChild(TagsEscolhidas),
		IonicPageModule.forChild(FiltroCamposTags),
		IonicPageModule.forChild(FiltroCampos),
		IonicPageModule.forChild(AdminTable),
		PipesModule
	],
	exports: [
		DiaSeletor,
		RefeicoesLista,
		RefeicoesEscolhidas,
		RefeicaoSeletor,
		ReceitasEscolhidas,
		TagsEscolhidas,
		FiltroCamposTags,
		FiltroCampos,
		AdminTable
	],
	schemas: []
})
export class ComponentsModule {}
