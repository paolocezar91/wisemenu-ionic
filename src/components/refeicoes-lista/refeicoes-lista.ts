import { Component, Input, Output, EventEmitter } from "@angular/core";
import { CurrentCardapiosProvider } from "../../providers/current-cardapios";

/*
	Generated class for the RefeicoesLista component.

	See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
	for more info on Angular 2 Components.
*/
@Component({
	selector: "refeicoes-lista",
	templateUrl: "refeicoes-lista.html"
})
export class RefeicoesLista {

	@Input() refeicoes: any
	@Input() refeicaoIndex: any
	@Output() refeicaoPage = new EventEmitter<any>();
	@Output() alterarRefeicao = new EventEmitter<any>();


	constructor(public ccp: CurrentCardapiosProvider) {
		console.log("Hello RefeicoesLista Component");
	}

	proximoIsSet() {
		return this.alterarRefeicao.observers.length > 0 && this.refeicoes[0].ordem < 5
	}

	anteriorIsSet() {
		return this.alterarRefeicao.observers.length > 0 && this.refeicoes[0].ordem > 0
	}

	alterarRefeicaoEmit(r, i) {
		if (r.ordem + i >= 0 && r.ordem + i <= 5) {
			this.alterarRefeicao.emit({r, i});
		}
	}

	refeicaoPageEmit(r, i) {
		this.refeicaoPage.emit({r, i})
	}

}
