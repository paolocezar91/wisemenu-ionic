import { Component, Input, Output, EventEmitter } from "@angular/core";

/*
	Generated class for the RefeicoesEscolhidas component.

	See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
	for more info on Angular 2 Components.
*/
@Component({
	selector: "tags-escolhidas",
	templateUrl: "tags-escolhidas.html"
})
export class TagsEscolhidas {

	@Input() tipo: any
	@Input() tagsEscolhidas: any
	@Output() remover: any = new EventEmitter<any>();

	constructor() {
		console.log("Hello TagsEscolhidas Component");
	}

	removerEmit(t) {
		this.remover.emit({t});
	}

}
