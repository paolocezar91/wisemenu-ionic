import { Component, Input } from "@angular/core"
import { AlertController } from "ionic-angular"
/*
	Generated class for the RefeicoesEscolhidas component.

	See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
	for more info on Angular 2 Components.
*/
@Component({
	selector: "refeicoes-escolhidas",
	templateUrl: "refeicoes-escolhidas.html"
})
export class RefeicoesEscolhidas {

	@Input() refeicoesEscolhidas: any
	@Input() refeicoes: any

	constructor(
		private alertCtrl: AlertController,
	) {
		console.log("Hello RefeicoesEscolhidas Component")
	}

	remover(r, i) {
		const alert = this.alertCtrl.create({
			title: "Remover refeição?",
			message: "Remover <strong>" + r.titulo + "</strong>?",
			buttons: [
				{text: "Não"},
				{
					text: "Sim",
					handler: data => {
						this.refeicoesEscolhidas.splice(i, 1)
						this.refeicoes.push(r)
						this.refeicoes.sort((a, b) => {
							return (a.id > b.id ? 1 : 0)
						})
					}
				}
			]
		})
		alert.present()
	}
}
