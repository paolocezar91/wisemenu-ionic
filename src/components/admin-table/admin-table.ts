import { Component, Input, Output, EventEmitter } from "@angular/core"
import { AlertController, ToastController } from "ionic-angular"

/**
 * Generated class for the AdminTable component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
	selector: "admin-table",
	templateUrl: "admin-table.html"
})
export class AdminTable {

	@Input() items: any
	@Input() options: any
	@Output() setOptions = new EventEmitter<any>()
	@Output() editItem = new EventEmitter<any>()
	@Output() removeItem = new EventEmitter<any>()

	constructor(
		private alertCtrl: AlertController
	) {
		console.log("Hello AdminTable Component")
	}

	pages() {
		return Array.from(Array(this.items.pages).keys())
	}

	outline(p) {
		return (p !== (this.items.page - 1))
	}

	editItemEmit(item) {
		this.editItem.emit(item)
	}

	removeItemEmit(item) {
		this.alertCtrl.create({
			title: "Remover item",
			message: "Você deseja remover este item?",
			buttons: [
				{
					text: "Não",
					role: "cancel",
				},
				{
					text: "Sim",
					handler: () => {
						this.removeItem.emit(item)
					}
				}
			]
		}).present();
	}

	setPageEmit(page) {
		this.options.page = Number(page + 1)
		this.setOptions.emit(this.options)
	}

	setLimitEmit($event) {
		this.options.limit = Number($event.target.value)
		this.options.page = Number(1)

		this.setOptions.emit(this.options)
	}

}
