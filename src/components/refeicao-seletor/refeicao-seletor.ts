import { Component, Input, Output, EventEmitter } from "@angular/core";

/**
 * Generated class for the RefeicaoSeletor component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
	selector: "refeicao-seletor",
	templateUrl: "refeicao-seletor.html"
})
export class RefeicaoSeletor {

	@Input() refeicoes: any
	@Input() refeicao: any
	@Input() refeicaoIndex: any
	@Output() refeicaoPage = new EventEmitter<any>()
	@Output() alterarRefeicao = new EventEmitter<any>()
	seletor: Boolean = false


	constructor() {
		console.log("Hello RefeicaoSeletor Component");
	}

	proximoIsSet() {
		return this.alterarRefeicao.observers.length > 0 && this.refeicao[0].ordem < 5
	}

	anteriorIsSet() {
		return this.alterarRefeicao.observers.length > 0 && this.refeicao[0].ordem > 0
	}

	alterarRefeicaoEmit(r, i) {
		this.seletor = false
		this.alterarRefeicao.emit({r, i});
	}

	refeicaoPageEmit(r, i) {
		this.refeicaoPage.emit({r, i})
	}

	toggleSeletor() {
		this.seletor = !this.seletor
	}

}
