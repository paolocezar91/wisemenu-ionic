import { Component, Input } from "@angular/core"
import { FilterArrayPipe } from "../../pipes/filter-array"

import _ from "lodash"

/*
	Generated class for the FiltroCampos component.

	See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
	for more info on Angular 2 Components.
*/
@Component({
	selector: "filtro-campos",
	templateUrl: "filtro-campos.html",
	providers: [FilterArrayPipe]
})
export class FiltroCampos {

	@Input() filtro: any
	@Input() tipo_tags: any

	tags_normal: any
	tags_nacionalidade: any

	constructor(
		private fap: FilterArrayPipe
	) {
		console.log("Component Filtro Campos")
	}

	filterTipo(tags, campo, tipo) {
		return this.fap.transform(tags, campo, tipo)
	}

	filterTags(tags, tagsEscolhidas) {
		return _.filter(tags, function(obj){ return !_.find(tagsEscolhidas, obj) })
	}

	adicionarTag(tagsEscolhidas, r) {
		tagsEscolhidas.push(r)
		tagsEscolhidas.sort((a, b) => {
			return (a.id > b.id ? 1 : 0)
		})
	}

	removerTag(event) {
		this.filtro.tags = this.filtro.tags.filter(i => i !== event.t)
	}
}
