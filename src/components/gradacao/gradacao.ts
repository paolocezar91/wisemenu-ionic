import { Component, Input } from "@angular/core";

@Component({
	selector: "gradacao",
	templateUrl: "gradacao.html",
})
export class Gradacao {
	public range: Array<number> = [1, 2, 3, 4, 5];
	@Input() grau: number
	@Input() icone: any
	@Input() image: any
	public arr = Array;

	constructor() {}
}