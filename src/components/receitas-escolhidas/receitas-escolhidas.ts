import { Component, Input } from "@angular/core";
import { ModalController, AlertController, ToastController } from "ionic-angular";

/*
	Generated class for the ReceitasEscolhidas component.

	See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
	for more info on Angular 2 Components.
*/
@Component({
	selector: "receitas-escolhidas",
	templateUrl: "receitas-escolhidas.html"
})
export class ReceitasEscolhidas {

	@Input() modelCardapioDia: any
	@Input() refeicaoIndex: any
	@Input() cardapio: any

	constructor(
		public modalCtrl: ModalController,
		private alertCtrl: AlertController,
		private toastCtrl: ToastController
	) {
		console.log("Hello ReceitasEscolhidas Component");
	}

	removerReceita(r) {
		const alert = this.alertCtrl.create({
				title: "Remover receita?",
				message: "Remover <strong>" + r.titulo + "</strong>?",
				buttons: [
					{text: "Não"},
					{
						text: "Sim",
						handler: data => {
							this.modelCardapioDia.refeicoes[this.refeicaoIndex].receitas = this.modelCardapioDia.refeicoes[this.refeicaoIndex].receitas.filter(i => i !== r);
							if (this.modelCardapioDia.refeicoes[this.refeicaoIndex].receitas.length === 0) {
								this.modelCardapioDia.refeicoes[this.refeicaoIndex].touched = false;
							}
							this.showToast("Receita removida");
						}
					}
				]
			});

		alert.present();
	}

	receitaModal(r) {
		const modal = this.modalCtrl.create("ReceitaPage", {
			receita: r,
		});
		modal.onDidDismiss(data => {
			console.log(data);
		});
		modal.present();
	}

	alterarPorcaoReceita(r) {
		const prompt = this.alertCtrl.create({
			title: "Quantidade de porções",
			message: "Inserir a quantidade de porções que deseja como padrão para cada prato escolhido.",
			inputs: [
				{
					name: "porcoes",
					type: "number",
				},
			],
			buttons: [
				{
					text: "Cancelar",
					handler: data => {}
				},
				{
					text: "OK",
					handler: data => {
						if (data.porcoes !== "") {
							r.porcoes = data.porcoes;
							this.showToast("Porção alterada");
						}
					}
				}
			]
		});
		prompt.present();
	}

	showToast(message) {
		const toast = this.toastCtrl.create({
			message,
			duration: 2000,
			position: "bottom",
			showCloseButton: true,
			closeButtonText: "X"
		});
		toast.present();
	}
}
