import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
		name: "filterArray",
		pure: false
})
export class FilterArrayPipe implements PipeTransform {

	transform(items: any[], field: string, value: any[]): any[] {
		if (!items) return [];
		let its = [];
		for (let i = value.length - 1; i >= 0; i--) {
			const filter = items.filter(it => it[field] === value[i]);
			its = its.concat( filter );
		}
		return its;
	}
}