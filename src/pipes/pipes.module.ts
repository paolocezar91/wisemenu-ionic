import { NgModule } from "@angular/core";

import { StringToDatePipe } from "./string-to-date";
import { FilterArrayPipe } from "./filter-array";
import { GroupByPipe } from "./group-by";
import { BoldPrefixPipe } from "./bold-prefix";
import { SeparatorPipe } from "./separator";

@NgModule({
	declarations: [
		FilterArrayPipe,
		GroupByPipe,
		StringToDatePipe,
		BoldPrefixPipe,
		SeparatorPipe
	],
	imports: [],
	exports: [
		FilterArrayPipe,
		GroupByPipe,
		StringToDatePipe,
		BoldPrefixPipe,
		SeparatorPipe
	]
})
export class PipesModule {
	constructor() {
		console.log("PipeModule");
	}
}
